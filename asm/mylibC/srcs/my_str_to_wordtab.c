/*
** my_str_to_wordtab.c for my_str_to_wordtab in /home/boguta_m/rendu/MUL_2013_fdf/mylibC/srcs
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Dec  5 02:14:26 2013 maxime boguta
** Last update Mon Dec  9 16:19:44 2013 maxime boguta
*/

#include <stdlib.h>
#include "my.h"

int	my_str_is_alphanum(char c, char sep)
{
  if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
    return (1);
  else if (c == sep)
    return (0);
  return (1);
}

int	my_str_count_word(char *s, char sep)
{
  int   i;
  int   cpt;

  i = 0;
  cpt = 0;
  while (s && s[i])
    {
      if (my_str_is_alphanum(s[i], sep))
        {
          cpt++;
          while (s && s[i] && my_str_is_alphanum(s[i], sep))
            i++;
        }
      else
	i++;
    }
  return (cpt);
}

int	my_str_count_char(char *s, int *i, char sep)
{
  int   cpt;

  cpt = 0;
  while (s[*i] && my_str_is_alphanum(s[*i], sep))
    {
      cpt++;
      (*i)++;
    }
  return (cpt);
}

char    **my_str_to_wordtab(char *str, int *cnt, char sep)
{
  int   i;
  int   j;
  int   word;
  int	cmp;
  char  **tab;

  i = 0;
  j = 0;
  word = my_str_count_word(str, sep);
  *cnt = word;
  tab = malloc((1 + word) * sizeof(char *));
  while (str && str[i] && word > 0)
    {
      if (my_str_is_alphanum(str[i], sep))
        {
	  tab[j] = &str[i];
	  tab[j++][my_str_count_char(str, &i, sep)] = 0;
          word--;
        }
      i++;
    }
  tab[*cnt] = NULL;
  return (tab);
}
