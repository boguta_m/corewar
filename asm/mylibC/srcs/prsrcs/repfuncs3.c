/*
** repfuncs3.c for my_printf in /home/boguta_m/Projets/PSU_2013_my_printf/srcs/prsrcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Sat Nov 16 21:47:38 2013 maxime boguta
** Last update Sun Nov 17 21:31:27 2013 maxime boguta
*/

#include <stdarg.h>
#include "def.h"
#include "my.h"

int		pr_bin(va_list *ap, t_flags *flags)
{
  return (my_putnbr_base_c(va_arg(ap, int), "01"));
}

int		pr_hexlow(va_list *ap, t_flags *flags)
{
  return (my_putnbr_base_c(va_arg(ap, int), "0123456789abcdef"));
}

int		pr_hexmaj(va_list *ap, t_flags *flags)
{
  return (my_putnbr_base_c(va_arg(ap, int), "0123456789ABCDEF"));
}

int		pr_stroct_calc(char *str, int i)
{
  int		ct;

  ct = 0;
  i = 0;
  while (str[i] != 0)
    {
      if (str[i] < 32 || str[i] >= 127)
	{
	  my_putchar('\\');
	  ct++;
	  if (str[i] <= 9)
	    {
	      my_putchar('0');
	      ct++;
	    }
	  ct = ct + my_putnbr_base_c(str[i], "012345678");
	}
      else
	{
	  my_putchar(str[i]);
	  ct++;
	}
      i++;
    }
  return (ct);
}

int		pr_stroct(va_list *ap, t_flags *flags)
{
  char		*str;
  int		i;

  i = 0;
  str = my_strdup(va_arg(ap, char *));
  return (pr_stroct_calc(str, i));
}
