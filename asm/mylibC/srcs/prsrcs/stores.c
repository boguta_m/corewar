/*
** stores.c for my_printf in /home/boguta_m/Projets/PSU_2013_my_printf/srcs/prsrcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Nov 14 20:21:31 2013 maxime boguta
** Last update Sun Nov 17 20:53:57 2013 maxime boguta
*/

#include <stdarg.h>
#include <stdlib.h>
#include "def.h"
#include "my.h"

void		store_struct(t_flags *flags)
{
  flags->atypes = my_strdup("discobxXSup");
  flags->globlen = 0;
  flags->wide = 0;
}

void		store_flags_rp(t_flags *flags)
{
  F = malloc(sizeof(int *) * 11);
  F[0] = pr_putnbr;
  F[1] = F[0];
  F[2] = pr_putstr;
  F[3] = pr_putchar;
  F[4] = pr_oct;
  F[5] = pr_bin;
  F[6] = pr_hexlow;
  F[7] = pr_hexmaj;
  F[8] = pr_stroct;
  F[9] = pr_putnbr_u;
  F[10] = pr_hexptr;
}
