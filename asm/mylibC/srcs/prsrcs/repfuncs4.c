/*
** repfuncs4.c for my_printf in /home/boguta_m/Projets/PSU_2013_my_printf/srcs/prsrcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Sun Nov 17 17:10:01 2013 maxime boguta
** Last update Sun Nov 17 21:01:37 2013 maxime boguta
*/

#include <stdarg.h>
#include "def.h"

int		pr_putnbr_u(va_list *ap, t_flags *flags)
{
  unsigned int	nb;
  char		print;
  int		div;
  int		ct;

  ct = 0;
  div = 1;
  nb = va_arg(ap, unsigned int);
  while (div < nb)
    div = div * 10;
  div = div / 10;
  while (div > 1)
    {
      if (nb < 0)
	{
	  write(1, '-', 1);
	  nb = -nb;
	  ct++;
	}
      print = ((nb / div) % 10) + '0';
      write(1, &print, 1);
      div = div / 10;
    }
  return (ct);
}

int		my_putnbr_base_l(long nbr, char *base, int *ct)
{
  long long     div;
  long		len;
  char		c;

  div = 1;
  len = my_strlen(base);
  if (nbr < 0)
    {
      my_putchar('-');
      *ct = *ct + 1;
      nbr = -nbr;
    }
  while (nbr / div > 1)
    div = div * len;
  div = div * len;
  while (div > 1)
    {
      div = div / len;
      c = nbr / div % len;
      my_putchar(base[c]);
      *ct = *ct + 1;
      if (*ct == 1)
	my_putchar('x');
    }
}

int		pr_hexptr(va_list *ap, t_flags *flags)
{
  long		nb;
  int		ct;
  int		*c;

  ct = 0;
  c = &ct;
  nb = va_arg(ap, long);
  my_putnbr_base_l(nb, "0123456789abcdef", c);
  return (ct + 1);
}
