/*
** my_strncmp.c for my_strncmp in /home/boguta_m/rendu/Piscine-C-Jour_06/ex_06
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Mon Oct  7 22:02:25 2013 maxime boguta
** Last update Wed Dec 11 21:35:46 2013 maxime boguta
*/

#include <stdlib.h>

int	my_strncmp(char *s1, char *s2, int n)
{
  int   i;

  i = 0;
  if (s1 == NULL || s2 == NULL)
    return (-2);
  while ((s1[i] || s2[i]) && i < n)
    {
      if (s1[i] < s2[i])
        return (-1);
      if (s1[i] > s2[i])
        return (1);
      i = i + 1;
    }
  if (s1[1] == 0 || s2[1] == 0)
    return (0);
  if ((s1[i] < s2[i]) && i < n)
    return (-1);
  if ((s1[i] > s2[i]) && i < n)
    return (1);
  return (0);
}
