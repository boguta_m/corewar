/*
** my_char_islower.c for my_char_islower in /home/boguta_m/rendu/lib/srcs/ncsrcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Nov 28 16:20:47 2013 maxime boguta
** Last update Thu Nov 28 17:08:25 2013 maxime boguta
*/

int	my_char_islower(char c)
{
  if (c >= 'a' && c <= 'z')
    return (1);
  else
    return (0);
}
