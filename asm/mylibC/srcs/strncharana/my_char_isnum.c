/*
** my_char_isnum.c for my_char_isnum in /home/boguta_m/rendu/lib/srcs/ncsrcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Nov 28 16:19:11 2013 maxime boguta
** Last update Thu Nov 28 17:08:38 2013 maxime boguta
*/

int	my_char_isnum(char c)
{
  if (c <= '9' && c >= '0')
    return (1);
  else
    return (0);
}
