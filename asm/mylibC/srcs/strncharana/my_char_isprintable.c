/*
** my_char_isprintable.c for my_char_isprintable in /home/boguta_m/rendu/lib/srcs/ncsrcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Nov 28 16:21:03 2013 maxime boguta
** Last update Thu Nov 28 16:22:55 2013 maxime boguta
*/

int	my_char_isprintable(char c)
{
  if (c <= 31 || c == 127)
    return (0);
  else
    return (1);
}
