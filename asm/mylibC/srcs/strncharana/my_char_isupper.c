/*
** my_char_isupper.c for my_char_isupper in /home/boguta_m/rendu/lib/srcs/ncsrcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Nov 28 16:21:51 2013 maxime boguta
** Last update Thu Nov 28 17:08:07 2013 maxime boguta
*/

int	my_char_isupper(char c)
{
  if (c >= 'A' && c <= 'Z')
    return (1);
  else
    return (0);
}
