/*
** my_char_isalpha.c for my_char_isalpha in /home/boguta_m/rendu/lib/srcs/ncsrcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Nov 28 16:16:57 2013 maxime boguta
** Last update Thu Nov 28 16:24:38 2013 maxime boguta
*/

int	my_char_isalpha(char c)
{
  if ((c < 'a' && c > 'Z') || c < 'A' || c > 'z')
    return (0);
  else
    return (1);
}
