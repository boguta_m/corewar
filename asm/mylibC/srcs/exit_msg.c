/*
** exit_msg.c for exit_msg in /home/boguta_m/rendu/mylibC/srcs
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Feb 20 13:06:54 2014 Maxime Boguta
** Last update Thu Feb 20 13:12:18 2014 Maxime Boguta
*/

#include <string.h>
#include <stdlib.h>

void	exit_msg(char *_msg, int err)
{
  if (_msg != NULL)
    {
      my_putstr(_msg);
      my_putchar('\n');
      exit(err);
    }
}
