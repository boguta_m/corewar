/*
** get_next_line.c for sudoki-bi in /home/boguta_m/rendu/sudoki-bi
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sat Mar  1 16:39:11 2014 Maxime Boguta
** Last update Thu Apr 10 17:03:26 2014 Maxime Boguta
*/

#include "my.h"
#include "get_next_line.h"

char		*is_there_n(char **line)
{
  char		*ret;
  char		*new;
  int		i;

  i = -1;
  ret = NULL;
  if (*line)
    while ((*line)[++i])
      if ((*line)[i] == '\n')
	{
	  (*line)[i] = 0;
	  ret = my_strdup(*line);
	  new = my_strdup(&((*line)[i + 1]));
	  free(*line);
	  *line = new;
	  return (ret);
	}
  return (ret);
}

char		*alloc_and_add(char *line, char *buf)
{
  if (line == NULL)
    {
      line = myxlloc(RD_AMT + 1);
      line[0] = 0;
    }
  else
    line = my_realloc(line, my_strlen(line) + my_strlen(buf) + 1,
		      my_strlen(line));
  my_strcat(line, buf);
  return (line);
}

int		f_clean(int clean, char **line)
{
  if (clean == 1)
    {
      if (*line)
	free(*line);
      *line = NULL;
      return (1);
    }
  return (0);
}

char		*get_next_line(int fd, int clean)
{
  static char	*line = NULL;
  char		*ret;
  char		buf[RD_AMT];
  int		rd;

  if (f_clean(clean, &line) == 1)
    return (NULL);
  if ((ret = is_there_n(&line)) != NULL)
    return (ret);
  while ((rd = read(fd, buf, RD_AMT)) > 0)
    {
      buf[rd] = 0;
      line = alloc_and_add(line, buf);
      if ((ret = is_there_n(&line)) != NULL)
	return (ret);
    }
  if (line)
    {
      free(line);
      line = NULL;
    }
  return (NULL);
}
