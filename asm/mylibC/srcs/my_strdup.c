/*
** my_strdup.c for my_strdup in /home/boguta_m/rendu/Piscine-C-Jour_08/ex_01
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Oct 10 10:10:13 2013 maxime boguta
** Last update Tue Jan  7 21:49:58 2014 Maxime Boguta
*/

#include <stdlib.h>
#include "my.h"

char	*my_strdup(char *src)
{
  int	l;
  char	*strdupped;

  if (src == NULL)
    return (NULL);
  l = my_strlen(src);
  strdupped = malloc((l + 2) * sizeof(char));
  if (strdupped == NULL)
    return (NULL);
  strdupped = my_strcpy(strdupped, src);
  return (strdupped);
}
