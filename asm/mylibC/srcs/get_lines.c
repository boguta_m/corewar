/*
** get_lines.c for get_lines in /home/boguta_m/rendu/mylibC/srcs
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Dec  5 01:56:48 2013 maxime boguta
** Last update Mon Mar 24 14:49:08 2014 Maxime Boguta
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include "my.h"

int	get_lines(char *path)
{
  int	fd;
  int	i;

  i = 0;
  if ((fd = open(path, O_RDONLY)) == -1)
     return (-1);
  while (get_next_line(fd, 0) != NULL)
    i++;
  if (close(fd) == -1)
    return (-1);
  return (i);
}
