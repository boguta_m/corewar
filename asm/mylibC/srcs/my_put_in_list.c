/*
** my_put_in_list.c for my_put_in_list in /home/boguta_m/rendu/lib/srcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Sun Nov 24 19:16:57 2013 maxime boguta
** Last update Thu Nov 28 22:25:49 2013 maxime boguta
*/

#include <stdlib.h>
#include "my.h"

t_list		*my_vladimir_put_in_list(t_list *in, void *data)
{
  t_list	*elem;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (NULL);
  if (in == NULL)
    {
      in = elem;
      elem->next = NULL;
      elem->prev = NULL;
      elem->data = data;
      return (elem);
    }
  while (in->next != NULL)
    in = in->next;
  elem->data = data;
  in->next = elem;
  elem->next = NULL;
  elem->prev = in;
  return (elem);
}
