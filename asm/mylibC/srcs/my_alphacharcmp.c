/*
** my_alphacharcmp.c for my_alphacharcmp in /home/boguta_m/rendu/lib/srcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Nov 28 17:06:44 2013 maxime boguta
** Last update Thu Nov 28 17:50:48 2013 maxime boguta
*/

int	my_alphacharcmp(char s1, char s2)
{
  char	v1;
  char	v2;

  v1 = s1;
  v2 = s2;
  if (my_char_isalpha(s1) == 0 || my_char_isalpha(s2) == 0)
    return (0);
  if (my_char_isupper(s1) == 1)
    v1 = s1 + 32;
  if (my_char_isupper(s2) == 1)
    v2 = s2 + 32;
  return (my_charcmp(v1, v2));
}
