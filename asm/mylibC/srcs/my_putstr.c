/*
** my_putstr.c for my_putstr in /home/boguta_m/rendu/Piscine-C-Jour_04
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Fri Oct  4 12:27:24 2013 maxime boguta
** Last update Fri Oct 25 12:00:45 2013 maxime boguta
*/

int	my_putstr(char *str)
{
  while (*str != '\0')
    {
      my_putchar(*str);
      str = str + 1;
    }
}
