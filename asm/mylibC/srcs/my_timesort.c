/*
** my_timesort.c for my_timesort in /home/boguta_m/rendu/lib/srcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Nov 28 22:11:48 2013 maxime boguta
** Last update Fri Nov 29 16:06:52 2013 maxime boguta
*/

#include <time.h>

int		my_timesort(time_t t1, time_t t2)
{
  time_t	tt1;
  time_t	tt2;

  tt1 = time(&t1);
  tt2 = time(&t2);
  if (tt1 < tt2)
    return (1);
  if (tt2 < tt1)
    return (-1);
  else
    return (0);
}
