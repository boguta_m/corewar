/*
** my_count_list.c for my_count_list in /home/boguta_m/rendu/lib/srcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Wed Nov 27 19:20:24 2013 maxime boguta
** Last update Wed Nov 27 19:25:28 2013 maxime boguta
*/

#include <stdlib.h>
#include "my.h"

int	my_count_list(t_list *list)
{
  int	ct;

  ct = 0;
  while (list != NULL)
    {
      ct++;
      list = list->next;
    }
  return (ct);
}
