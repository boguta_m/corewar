/*
** wordtab2.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Feb 18 19:03:13 2014 camill_n
** Last update Wed Apr  9 16:33:20 2014 Maxime Boguta
*/

#include "my.h"

char	*my_strncatt(char *s1, int i, int j)
{
  char	*new;
  int	k;

  k = 0;
  new  = myxlloc((j - i + 1) * sizeof(char));
  while (i < j)
    new[k++] = s1[i++];
  new[k] = 0;
  return (new);
}

char	**my_wordtab_e(char *av, char sep)
{
  char	**tab;
  int	nb_word;
  int	i;
  int	tmp;
  int	cpt;

  i = __init_wd_e(&nb_word, av, &cpt, &tab);
  while (av[i] != '\0')
    {
      while (av[i] == sep && av[i] != '\0')
        ++i;
      tmp = i;
      while (av[tmp] != '\0' && (av[tmp] != sep))
        ++tmp;
      if (tmp - i > 0)
	{
	  tab[cpt] = my_strncatt(av, i, tmp);
	  ++cpt;
	}
      i = tmp;
    }
  tab[cpt] = NULL;
  return (tab);
}
