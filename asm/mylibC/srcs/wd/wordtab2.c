/*
** wordtab2.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Feb 18 19:03:13 2014 camill_n
** Last update Wed Apr  9 16:37:02 2014 Maxime Boguta
*/

#include "global.h"

int	get_nb_word_e(char *str, char c)
{
  int	i;
  int	cpt;

  cpt = 0;
  i = 0;
  while (str[i] != '\0')
    {
      (str[i] == c && str[i + 1] != c) ? ++cpt : 0;
      ++i;
    }
  return (cpt);
}

int	__init_wd_e(int *nb_word, char *av, int *cpt, char ***wd)
{
  *nb_word = get_nb_word_e(av, '"');
  *wd = myxlloc((*nb_word + 1) * sizeof(char *));
  *cpt = 0;
  return (0);
}

int	__init_wd(int *nb_word, char *av, int *cpt, char ***wd)
{
  *nb_word = get_nb_word(av, ',');
  *wd = myxlloc((*nb_word + 1) * sizeof(char *));
  *cpt = 0;
  return (0);
}

char	**my_wordtab(char *av, char sep)
{
  char	**tab;
  int	nb_word;
  int	i;
  int	tmp;
  int	cpt;

  i = __init_wd(&nb_word, av, &cpt, &tab);
  while (av[i] != '\0')
    {
      while ((av[i] == ' ' || av[i] == '\t' || av[i] == sep) && av[i] != '\0')
        ++i;
      tmp = i;
      while (av[tmp] != '\0' && (av[tmp] != sep &&
				 av[tmp] != ' ' && av[tmp] != '\t'))
        ++tmp;
      if (tmp - i > 0)
	{
	  tab[cpt] = my_strncatt(av, i, tmp);
	  ++cpt;
	}
      i = tmp;
    }
  tab[cpt] = NULL;
  return (tab);
}
