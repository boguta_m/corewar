/*
** my_strcapitalize.c for my_strcapitalize in /home/boguta_m/rendu/Piscine-C-Jour_06/ex_09
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct  7 22:01:00 2013 maxime boguta
** Last update Fri Nov 22 23:34:30 2013 maxime boguta
*/

char	*my_strcapitalize(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[0] >= 97 && str[0] <= 122)
	str[0] = str[0] - 32;
      if (str[i + 1] >= 97 && str[i + 1] <= 122)
	{
	  if ((str[i] < 'a' && str[i] > 'Z') || str[i] < 'A' || str[i] > 'z')
	    str[i + 1] = str[i + 1] - 32;
	}
      i = i + 1;
    }
  return (str);
}
