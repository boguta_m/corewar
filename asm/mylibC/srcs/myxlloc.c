/*
** myxlloc.c for my_xmalloc in /home/boguta_m/rendu/mylibC/srcs
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Feb 20 13:04:56 2014 Maxime Boguta
** Last update Thu Feb 20 13:11:57 2014 Maxime Boguta
*/

#include <stdlib.h>

void	*myxlloc(size_t size)
{
  void	*_rt;

  if ((_rt = malloc(size)) == NULL)
    exit_msg("error : memory allocation failed.", 1);
  return (_rt);
}
