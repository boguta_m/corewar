/*
** my_count_wordtab.c for my_count_wordtab in /home/boguta_m/rendu/mylibC/srcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Tue Dec  3 15:53:17 2013 maxime boguta
** Last update Tue Dec  3 16:00:15 2013 maxime boguta
*/

int	my_count_wordtab(char **wdtab)
{
  int	i;

  i = 0;
  while (*wdtab)
    {
      wdtab++;
      i++;
    }
  return (i);
}
