/*
** global.h for my_select in /home/camill_n/rendu/PSU_2013_my_select
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Sat Jan 11 00:01:31 2014 Nicolas Camilli
** Last update Wed Mar 12 12:06:32 2014 Maxime Boguta
*/

#ifndef GLOBAL_H_
# define GLOBAL_H_

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include "common.h"
# include "func.h"
# include "alloc.h"
# include "complement.h"
# include "errors.h"
# include "get_next_line.h"
# include "parse.h"
# include "wordtab.h"
# include "asm.h"
# include "op.h"
# include "init.h"

#endif
