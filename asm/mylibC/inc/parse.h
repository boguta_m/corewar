/*
** parse.h for corewar in /home/boguta_m/rendu/CPE_2013_corewar
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Tue Mar  4 15:28:06 2014 Maxime Boguta
** Last update Fri Mar 21 14:57:45 2014 Maxime Boguta
*/

#ifndef PARSE_H_
# define PARSE_H_

char	*get_next_line(int fd, int flag);
void	get_params(int fd, int fdout);

#endif
