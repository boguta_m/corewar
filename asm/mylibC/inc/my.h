/*
** libmy.h for libmy_header in /home/boguta_m/rendu/Piscine-C-include
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Oct 10 11:25:44 2013 maxime boguta
** Last update Wed Mar 26 17:00:33 2014 Maxime Boguta
*/

#ifndef MY_H_
# define MY_H_
# include <stdlib.h>

typedef struct	s_list
{
  struct s_list	*next;
  struct s_list	*prev;
  void		*data;
}		t_list;

void	my_putchar(char c);
int	my_isneg(int nb);
int	my_put_nbr(int nb);
int	my_swap(int *a, int *b);
int	my_putstr(char *str);
int	my_strlen(char *str);
int	my_getnbr(char *str);
void	my_sort_int_tab(int *tab, int size);
int	my_power_rec(int nb, int power);
int	my_square_root(int nb);
int	my_is_prime(int nombre);
int	my_find_prime_sup(int nb);
char	*my_strcpy(char *dest, char *src);
char	*my_strncpy(char *dest, char *src, int nb);
char	*my_revstr(char *str);
char	*my_strstr(char *str, char *to_find);
int	my_strcmp(char *s1, char *s2);
int	my_strncmp(char *s1, char *s2, int nb);
char	*my_strupcase(char *str);
char	*my_strlowcase(char *str);
char	*my_strcapitalize(char *str);
int	my_str_isalpha(char *str);
int	my_str_isnum(char *str);
int	my_str_islower(char *str);
int	my_str_isupper(char *str);
int	my_str_isprintable(char *str);
int	my_showstr(char *str);
int	my_showmem(char *str, int size);
char	*my_strcat(char *dest, char *src);
char	*my_strncat(char *dest, char *src, int nb);
int	my_strlcat(char *dest, char *src, int size);
int	my_putnbr_base(char *str, char *base);
char	**my_str_to_wordtab(char *str, int *ct, char sep);
char	*my_strdup(char *str);
void	*my_calloc(int size, char fill);
t_list	*my_put_in_list(t_list *list, void *data);
char	*get_next_line(int fd, int flag);
char	*my_getenv(char *env, char *envp[]);
void	exit_msg(char *m, int er);
void	*myxlloc(size_t size);
char	*my_strncatt(char *, int, int);
void	*my_realloc(void *, size_t ns, size_t os);

#endif
