/*
** my_put_in_list.h for my_put_in_list in /home/boguta_m/rendu/lib/inc
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sun Nov 24 19:28:31 2013 maxime boguta
** Last update Thu Feb 20 13:10:40 2014 Maxime Boguta
*/

#ifndef MY_PUT_IN_LIST_H
# define MY_PUT_IN_LIST_H

typedef struct s_struct
{
  void			*list;
  struct s_struct	*next;
  struct s_struct	*prev;
} t_list;

#endif
