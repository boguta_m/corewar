/*
** init.h for init in /home/camill_n/rendu/MUL_2013_rtv1
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Wed Feb  5 16:29:48 2014 Nicolas Camilli
** Last update Wed Mar 12 12:07:29 2014 Maxime Boguta
*/

#ifndef INIT_H_
# define INIT_H_

# include "global.h"

void	init_parse(t_glo *glo);

#endif
