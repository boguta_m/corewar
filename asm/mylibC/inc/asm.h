/*
** asm.h for asm in /home/bousca_a/Documents/asm/includes
**
** Made by bousca_a
** Login   <bousca_a@epitech.net>
**
** Started on  Wed Feb 19 13:12:35 2014 bousca_a
** Last update Thu Feb 27 17:50:13 2014 antonin bouscarel
*/

#ifndef ASM_H_
# define ASM_H_

int	get_fd(char *file);
int	get_file(int fd);
void	make_asm(char *file);

#endif
