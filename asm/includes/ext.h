/*
** ext.h for corewar in /home/boguta_m/rendu/CPE_2013_corewar/asm
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sun Apr  6 15:34:48 2014 Maxime Boguta
** Last update Sun Apr  6 15:35:05 2014 Maxime Boguta
*/

#ifndef EXT_H_
# define EXT_H

#include "op.h"

extern op_t	op_tab[];

#endif
