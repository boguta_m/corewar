/*
** labels.c for corewar in /home/boguta_m/rendu/CPE_2013_corewar/asm/src
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Tue Mar  4 17:42:15 2014 Maxime Boguta
** Last update Wed Apr  9 18:33:31 2014 Maxime Boguta
*/

#include "list.h"
#include "global.h"
#include "ext.h"

void	store_label(t_glo *glo, char *line, int tot, int l)
{
  se_for_label(glo, line, l);
  glo->label = my_vladimir_put_in_list(glo->label);
  glo->label->name = my_strdup(line);
  glo->label->name[my_strlen(line) - 1] = 0;
  glo->label->pos = tot;
}

void	chk_lab_chars(char **line, int l, int size, t_glo *glo)
{
  int	i;

  i = -1;
  while (LABEL_CHARS[++i] != 0 && LABEL_CHARS[i] != line[0][size]);
  if (LABEL_CHARS[i] == 0)
    exit_line("error : label can't be defined in LABEL_CHARS at line",
	      l, 1, glo);
}

char	**chk_if_label(t_glo *glo, char **line, int tot)
{
  if (label_ok(line, glo->l, glo) == 1)
    {
      store_label(glo, line[0], tot, glo->l);
      return (++line);
    }
  return (line);
}

int	chk_if_ins(t_glo *glo, char **line)
{
  int	i;

  i = -1;
  if (line && line[0] && line[0][0] != 0)
    {
      while (++i < 16 && my_strcmp(op_tab[i].mnemonique, line[0]) != 0);
      if (i >= 16)
	{
	  my_printf("error : instruction '%s' ", line[0]);
	  exit_line("doesn't exit at line", glo->l, 1, glo);
	}
      line++;
      return (get_size(line, i, glo));
    }
  return (0);
}

void	get_labels(int fd, t_glo *glo)
{
  char	*line;
  char	**wt;
  char	**wtp;
  int	tot;

  glo->l = 0;
  tot = 0;
  while ((line = get_next_line(fd, 0)) != NULL)
    {
      if (line[0] != 0 && skip_comments(line) == 0)
	{
	  wt = my_wordtab(line, SEPARATOR_CHAR);
	  wtp = chk_if_label(glo, wt, tot);
	  tot += chk_if_ins(glo, wtp);
	  my_free_wordtab(wt);
	}
      free(line);
      glo->l++;
    }
}
