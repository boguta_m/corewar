/*
** init.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Feb 10 20:50:47 2014 camill_n
** Last update Sat Apr 12 16:53:03 2014 Maxime Boguta
*/

#include <string.h>
#include "common.h"
#include "init.h"
#include "my.h"

void	reinit_flags(t_glo *glo, char *l)
{
  free(l);
  glo->line.iflag = 0;
  glo->line.arg[0].aflag = 0;
  glo->line.arg[1].aflag = 0;
  glo->line.arg[2].aflag = 0;
  glo->line.arg[3].aflag = 0;
  glo->line.arg[0].val = 0;
  glo->line.arg[1].val = 0;
  glo->line.arg[2].val = 0;
  glo->line.arg[3].val = 0;
}

void	header_init(header_t *hea)
{
  int	i;
  char	*in;

  in = (char *)hea;
  i = -1;
  while (++i < sizeof(header_t))
    in[i] = 0;
}

void	init_parse(t_glo *glo)
{
  int	i;

  i = -1;
  glo->line.iflag = 0;
  glo->label = NULL;
  glo->output = myxlloc(my_strlen(glo->file) + 4);
  while (glo->file[++i] != '.' && glo->file[i])
    glo->output[i] = glo->file[i];
  glo->output[i] = 0;
  my_strcat(glo->output, ".cor");
}
