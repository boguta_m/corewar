/*
** display.c for corewar in /home/boguta_m/rendu/CPE_2014_corewar/asm
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sat Apr 12 16:28:33 2014 Maxime Boguta
** Last update Sat Apr 12 16:49:29 2014 Maxime Boguta
*/

#include "common.h"

void	_display(t_glo *glo)
{
  my_printf("\nCompiling input '%s' file  \n\n", glo->file);
  my_printf("\tName : '%s'\n", glo->header.prog_name);
  my_printf("\tComment : '%s'\n", glo->header.comment);
  my_printf("  \t\t\t\t  \033[32m[OK]\033[00m\n\n");
}
