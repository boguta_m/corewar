/*
** errors.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Feb 10 20:51:32 2014 camill_n
** Last update Thu Apr 10 16:44:58 2014 Maxime Boguta
*/

#include "global.h"

void	error_arg(char *name)
{
  my_printf("Usage: %s [file]\n", name);
  exit(EXIT_FAILURE);
}

void	error_malloc(char *str)
{
  my_printf("Memory access denied for var: %s\n", str);
  exit(0);
}

void	error_file_path(char *name)
{
  my_printf("Error, the file %s is not accessible\n", name);
  exit(0);
}

void	exit_msg(char *msg, int errcode)
{
  my_printf("%s\n", msg);
  exit(errcode);
}

void	exit_line(char *msg, int line, int errcode, t_glo *glo)
{
  my_printf("%s %d\n", msg, line);
  free_var(glo);
  exit(errcode);
}
