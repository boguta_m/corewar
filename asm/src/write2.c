/*
** write2.c for corewar in /home/boguta_m/rendu/CPE_2013_corewar/asm
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Apr 10 16:10:11 2014 Maxime Boguta
** Last update Sat Apr 12 15:17:51 2014 Maxime Boguta
*/

#include "my.h"
#include "common.h"

int		send_data(unsigned char *line, int size, t_glo *all)
{
  write(all->fdout, line, size);
}

unsigned char	*_init_line(unsigned char *line)
{
  int		i;

  i = 0;
  line = myxlloc(MAX_SIZE);
  while (i < MAX_SIZE)
    line[i++] = 0;
  return (line);
}
