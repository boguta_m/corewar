/*
** main.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Feb 10 20:50:26 2014 camill_n
** Last update Sun Apr 13 19:48:09 2014 Maxime Boguta
*/

#include "global.h"

int	main(int ac, char **av)
{
  int	i;

  i = 0;
  ac < 2 ? error_arg(av[0]) : 0;
  while (av[++i])
    {
      main_exec(ac, av[i]);
      if (av[i + 1])
	my_printf("============================================\n");
    }
  return (0);
}

int	main_exec(int ac, char *av)
{
  t_glo	glo;

  glo.file = av;
  glo.write = 0;
  glo.pc = 0;
  my_printf("\nFile '%s' : ", glo.file);
  my_printf("\nChecking for errors ...\n");
  main_parse(&glo);
  my_printf("\t\t                  \033[32m[OK]\033[00m\n");
  _display(&glo);
  free_var(&glo);
   glo.write = 1;
  main_parse(&glo);
  my_printf("\nCreating output '%s' file\n", glo.output);
  my_printf("\t\t\t\t  \033[32m[OK]\033[00m\n\n");
  free_var(&glo);
  return (0);
}
