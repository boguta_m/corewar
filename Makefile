##
## Makefile for test in /home/camill_n/rendu/Piscine-C-lib/my
##
## Made by Nicolas Camilli
## Login   <camill_n@epitech.net>
##
## Started on  Mon Oct 21 07:17:19 2013 Nicolas Camilli
## Last update Sun Apr 13 19:27:11 2014 Maxime Boguta
##

all:
	make -C corewar
	make -C asm

re :	fclean all

clean :
	make -C corewar clean
	make -C asm clean

fclean :
	make -C corewar fclean
	make -C asm fclean
