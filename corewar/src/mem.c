/*
** mem.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Mar 25 22:01:11 2014 camill_n
** Last update Sun Apr 13 14:55:42 2014 camill_n
*/

#include "global.h"

int		get_param(unsigned char param, int mode)
{
  int		ret;

  ret = 0;
  param >>= 6 - (mode * 2);
  ret = (param & 1);
  param >>= 1;
  ret += (param & 1) << 1;
  return (ret);
}

int		set_val(t_data *data, int pos, int src, int size)
{
  int		i;

  i = 0;
  pos += (size - 1);
  while (i < size)
    {
      VM->map[(pos - i) % MEM_SIZE] = (unsigned char)(src);
      src >>= 8;
      ++i;
    }
  return (0);
}

int		char_to_int(t_data *data, int *dst, int pos, int nb_bloc)
{
  int		j;
  int		i;
  unsigned char	tmp;

  j = 0;
  while (j < nb_bloc)
    {
      pos %= MEM_SIZE;
      i = 8;
      tmp = VM->map[pos];
      ++pos;
      while (i > 0)
	{
	  *dst += (tmp & 1) << ((nb_bloc * 8) - (j * 8) - i);
	  tmp >>= 1;
	  --i;
	}
      ++j;
    }
  return (0);
}

int		get_val(t_data *data, int type, int pos, t_process *proc)
{
  int		ret;
  int		tmp;

  ret = 0;
  tmp = 0;
  pos %= MEM_SIZE;
  if (type == REG_VAL && VM->map[pos] >= 1 && VM->map[pos] <= REG_NUMBER)
    ret = REG[(int)(VM->map[pos] - 1)];
  if (type == DIRECT_VAL)
    char_to_int(data, &ret, pos, DIR_SIZE);
  if (type == IND_REG_VAL)
    char_to_int(data, &ret, pos, IND_REG_SIZE);
  if (type == IND_DIR_VAL)
    char_to_int(data, &ret, pos, IND_SIZE);
  if (type == INDIRECT_VAL)
    {
      char_to_int(data, &tmp, pos, IND_SIZE);
      tmp %= IDX_MOD;
      char_to_int(data, &ret, proc->pc + tmp, IND_SIZE);
    }
  return (ret);
}

int		show_param(unsigned char param)
{
  int		i;

  i = 0;
  while (i < 4)
    {
      my_printf("Le parametre %d est: %d\n", i + 1, get_param(param, i));
      ++i;
    }
  return (0);
}
