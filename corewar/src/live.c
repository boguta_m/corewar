/*
** live.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Mar 24 14:41:26 2014 camill_n
** Last update Sun Apr 13 15:05:17 2014 camill_n
*/

#include "global.h"

int	live(t_data *data, t_process *proc)
{
  int	val;
  int	i;

  i = 0;
  val = get_val(data, DIRECT_VAL, POS + 1, proc);
  while (i < NB_PROG)
    {
      if (PROG_NUMBER[i] == val && PROG_LIVE[i] >= 0)
	{
	  my_printf("le joueur %s est en vie\n", HEADER[i].prog_name);
	  PROG_LIVE[i] = 0;
	  ++NB_LIVE;
	}
      ++i;
    }
  POS += 5;
  return (0);
}

int	inc_live(t_data *data)
{
  int	i;

  i = 0;
  while (i < NB_PROG && PROG_LIVE[i] >= 0)
    ++PROG_LIVE[i++];
  ++CYCLE_BEFORE_DIE;
  return (0);
}

int	check_live(t_data *data)
{
  int	i;

  if (CYCLE_BEFORE_DIE >= CYCLETODIE)
    {
      i = 0;
      while (i < NB_PROG)
	{
	  if (PROG_LIVE[i] >= CYCLETODIE)
	    {
	      my_printf("le champion %d n'est plus en vie\n", PROG_NUMBER[i]);
	      PROG_LIVE[i] = -1;
	    }
	  ++i;
	}
      CYCLE_BEFORE_DIE = 0;
    }
  if (NB_LIVE >= NBR_LIVE && STATE_LAST_LIVE == NB_PROG)
    CYCLETODIE -= CYCLE_DELTA;
  else
    STATE_LAST_LIVE = NB_PROG;
  NB_LIVE >= NBR_LIVE ? NB_LIVE = 0 : 0;
  return (0);
}
