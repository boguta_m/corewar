/*
** mem2.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed Mar 26 21:56:04 2014 camill_n
** Last update Sun Apr 13 14:32:15 2014 camill_n
*/

#include "global.h"

int	get_jump(t_data *data, unsigned char param, t_process *proc, int size)
{
  int	i;
  int	pas;
  int	tmp;

  i = 0;
  pas = 2;
  while (i < size)
    {
      tmp = get_param(VM->map[POS + 1], i);
      tmp == REG_VAL ? pas += 1 : 0;
      tmp == DIRECT_VAL ? pas += DIR_SIZE : 0;
      tmp == INDIRECT_VAL ? pas += IND_SIZE : 0;
      ++i;
    }
  return (pas);
}

int	get_all_param(t_data *data, int *type, int *arg, t_process *proc)
{
  int	i;
  int	pas;

  i = 0;
  pas = 2;
  while (i < 4)
    {
      type[i] = get_param(VM->map[POS + 1], i);
      if (arg != NULL)
	{
	  if (type[i] == REG_VAL)
	    arg[i] = get_val(data, IND_REG_VAL, POS + pas, proc);
	  else
	    arg[i] = get_val(data, type[i], POS + pas, proc);
	  type[i] == REG_VAL ? pas += 1 : 0;
	  type[i] == DIRECT_VAL ? pas += DIR_SIZE : 0;
	  type[i] == INDIRECT_VAL ? pas += IND_SIZE : 0;
	}
      ++i;
    }
  return (0);
}

int	get_ind_param(t_data *data, int *type, int *arg, t_process *proc)
{
  int	i;
  int	pas;

  i = 0;
  pas = 2;
  while (i < 4)
    {
      type[i] = get_param(VM->map[POS + 1], i);
      if (arg != NULL)
	{
	  if (type[i] == REG_VAL)
	    {
	      arg[i] = get_val(data, IND_REG_VAL, POS + pas, proc);
	      arg[i] >= 1 && arg[i] <= REG_NUMBER ? arg[i] = REG[arg[i] - 1] : 0;
	    }
	  else
	    arg[i] = get_val(data, IND_DIR_VAL, POS + pas, proc);
	  if (type[i] == INDIRECT_VAL)
	    arg[i] = get_val(data, IND_DIR_VAL,
			     (POS + (arg[i] % IDX_MOD)) % MEM_SIZE, proc);
	  pas += (type[i] == REG_VAL) ? 1 : IND_SIZE;
	}
      ++i;
    }
  return (0);
}
