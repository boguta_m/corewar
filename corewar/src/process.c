/*
** process.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Mar  2 17:08:12 2014 camill_n
** Last up Sun Mar  2 17:12:03 2014 camill_n
*/

#include "global.h"

int		add_proc(t_process **process, int champ_id, int pc, int *reg)
{
  t_process	*proc;
  t_process	*tmp;
  int		i;

  i = -1;
  tmp = process[0];
  proc = x_malloc(sizeof(t_process), "process");
  proc->next = NULL;
  proc->champ_id = champ_id;
  proc->pc = pc;
  proc->time_to_exec = -1;
  proc->carry = 0;
  while (++i < REG_NUMBER)
    proc->reg[i] = reg[i];
  if (process[0] == NULL)
    process[0] = proc;
  else
    {
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = proc;
    }
  return (0);
}

int		set_carry(t_process **process, int carry)
{
  t_process	*tmp;

  tmp = process[0];
  while (tmp->next != NULL)
    tmp = tmp->next;
  tmp->carry = carry;
  return (0);
}
