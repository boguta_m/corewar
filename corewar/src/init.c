/*
** init.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Feb 10 20:50:47 2014 camill_n
** Last update Sun Apr 13 14:54:38 2014 camill_n
*/

#include "global.h"

int	set_addr(t_data *data)
{
  int	size;
  int	i;

  i = 0;
  size = MEM_SIZE / NB_PROG;
  while (i < NB_PROG)
    {
      if (PROG_ADDR[i] < 0 || PROG_ADDR[i] >= MEM_SIZE)
	PROG_ADDR[i] = i * size;
      ++i;
    }
  return (0);
}

int	set_vm_func(t_vm_func *vm_f, unsigned char val,
		    int (*e)(t_data *data, t_process *proc), int time)
{
  vm_f->val = val;
  vm_f->time = time;
  vm_f->execute = e;
  return (0);
}

int	load_vm_func(t_data *data)
{
  set_vm_func(&VM_FUNC[0], 0x01, &live, 10);
  set_vm_func(&VM_FUNC[1], 0x02, &ld, 5);
  set_vm_func(&VM_FUNC[2], 0x03, &st, 5);
  set_vm_func(&VM_FUNC[3], 0x04, &add, 10);
  set_vm_func(&VM_FUNC[4], 0x05, &sub, 10);
  set_vm_func(&VM_FUNC[5], 0x06, &and, 6);
  set_vm_func(&VM_FUNC[6], 0x07, &or, 6);
  set_vm_func(&VM_FUNC[7], 0x08, &xxor, 6);
  set_vm_func(&VM_FUNC[8], 0x09, &zjmp, 20);
  set_vm_func(&VM_FUNC[9], 0x10, &aff, 2);
  set_vm_func(&VM_FUNC[10], 0x0a, &ldi, 25);
  set_vm_func(&VM_FUNC[11], 0x0b, &sti, 25);
  set_vm_func(&VM_FUNC[12], 0x0c, &vm_fork, 800);
  set_vm_func(&VM_FUNC[13], 0x0d, &lld, 10);
  set_vm_func(&VM_FUNC[14], 0x0e, &lldi, 50);
  set_vm_func(&VM_FUNC[15], 0x0f, &vm_lfork, 1000);
  return (0);
}

int	init_var(t_data *data, int *i, int *k)
{
  *i = 0;
  *k = 0;
  PROC = NULL;
  set_addr(data);
  CYCLETODIE = CYCLE_TO_DIE;
  CYCLE_BEFORE_DIE = 0;
  my_ubzero(VM->map, MEM_SIZE);
  return (0);
}

int	load_champ(t_data *data, char *path[])
{
  int	i;
  int	k;
  int	reg[REG_NUMBER];

  verif_number(data);
  init_var(data, &i, &k);
  k = 0;
  while (k < REG_NUMBER)
    reg[k++] = 0;
  while (i < NB_PROG)
    {
      make_bin(data, i, path);
      reg[0] = PROG_NUMBER[i];
      add_proc(&PROC, i, PROG_ADDR[i], reg);
      PROG_LIVE[i] = 0;
      ++VM->nb_proc;
      ++i;
    }
  NB_LIVE = 0;
  STATE_LAST_LIVE = NB_PROG;
  return (0);
}
