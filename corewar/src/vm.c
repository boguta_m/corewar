/*
** vm.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Mar  4 18:45:51 2014 camill_n
** Last update Sun Apr 13 15:06:25 2014 camill_n
*/

#include "global.h"

int		exec_ins(t_data *data, t_process *proc)
{
  int		i;

  i = 0;
  while (i < NB_INS)
    {
      if (VM_FUNC[i].val == VM->map[proc->pc])
	{
	  if (proc->time_to_exec == -1)
	    proc->time_to_exec = VM_FUNC[i].time - 1;
	  else if (proc->time_to_exec > 0)
	    --proc->time_to_exec;
	  else
	    {
	      verif_ctmo(data, VM_FUNC[i].val) == 0 ?
	      	VM_FUNC[i].execute(data, proc) : (proc->pc += 3);
	      proc->time_to_exec = -1;
	      (proc->pc >= MEM_SIZE) ? proc->pc = 0 : 0;
	    }
	  return (0);
	}
      ++i;
    }
  ++proc->pc;
  (proc->pc >= MEM_SIZE) ? proc->pc = 0 : 0;
  return (0);
}

int		exec_process(t_data *data)
{
  t_process	*proc;

  proc = PROC;
  while (proc != NULL)
    {
      if (PROG_LIVE[proc->champ_id] > -1)
	{
	  check_live(data);
	  exec_ins(data, proc);
	}
      proc = proc->next;
    }
  return (0);
}

int		check_winner(t_data *data, int dump)
{
  int		ret;
  int		id;
  int		i;

  ret = -1;
  id = -1;
  i = 0;
  while (i < NB_PROG)
    {
      if ((PROG_LIVE[i] < ret && PROG_LIVE[i] > -1) || ret == -1)
	{
	  ret = PROG_LIVE[i];
	  id = i;
	}
      ++i;
    }
  print_mem(VM->map, MEM_SIZE);
  my_printf("\n____________________\n\n");
  if (id >= 0 && id < NB_PROG)
    my_printf("Le joueur %d(%s) a gagne\n", PROG_NUMBER[id],
	      HEADER[id].prog_name);
  else
    my_printf("Problème lors de la creation du resultat\n");
  return (0);
}

int		check_nb_champ(t_data *data)
{
  int		i;
  int		cpt;

  cpt = 0;
  i = 0;
  while (i < NB_PROG)
    {
      if (PROG_LIVE[i] >= 0)
	++cpt;
      ++i;
    }
  return (cpt);
}

int		exec(t_data *data)
{
  int		dump;

  dump = 0;
  while (dump < DUMP && check_nb_champ(data) > 1 && CYCLETODIE > 100)
    {
      exec_process(data);
      inc_live(data);
      ++dump;
      if (VM->ctmo > 0)
	--VM->ctmo;
    }
  check_winner(data, dump);
  return (0);
}
