/*
** arithmetic.c for corewar in /home/camill_n/rendu/CPE_2014_corewar/src
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed Mar 26 11:56:38 2014 camill_n
** Last update Sat Apr 12 17:52:05 2014 camill_n
*/

#include "global.h"

int	add(t_data *data, t_process *proc)
{
  int	type[4];
  int	val[4];
  int	ret;

  get_all_param(data, type, val, proc);
  if (val[2] <= REG_NUMBER && val[2] >= 1)
    {
      if (type[0] == REG_VAL && val[0] <= REG_NUMBER && val[0] >= 1)
	ret = REG[val[0] - 1];
      else
	ret = val[0];
      if (type[1] == REG_VAL && val[1] <= REG_NUMBER && val[1] >= 1)
	ret += REG[val[1] - 1];
      else
	ret += val[1];
      REG[val[2] - 1] = ret;
      proc->carry = (ret == 0 ? 1 : -1);
    }
  proc->pc += get_jump(data, VM->map[(POS + 1) % MEM_SIZE], proc, 3);
  return (0);
}

int	sub(t_data *data, t_process *proc)
{
  int	type[4];
  int	val[4];
  int	ret;

  get_all_param(data, type, val, proc);
  if (val[2] <= REG_NUMBER && val[2] >= 1)
    {
      if (type[0] == REG_VAL && val[0] <= REG_NUMBER && val[0] >= 1)
	ret = REG[val[0] - 1];
      else
	ret = val[0];
      if (type[1] == REG_VAL && val[1] <= REG_NUMBER && val[1] >= 1)
	ret -= REG[val[1] - 1];
      else
	ret -= val[1];
      REG[val[2] - 1] = ret;
      proc->carry = (ret == 0 ? 1 : -1);
    }
  proc->pc += get_jump(data, VM->map[(POS + 1) % MEM_SIZE], proc, 3);
  return (0);
}
