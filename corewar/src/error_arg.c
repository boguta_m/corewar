/*
** error_arg.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Mar  2 17:08:12 2014 camill_n
** Last up Sun Mar  2 17:12:03 2014 camill_n
*/

#include "global.h"

void	error_arg(char *name)
{
  my_printf("Usage: %s [-dump nbr_cycle] [[-n prog_number] ", name);
  my_printf("[-a load_address ] prog_name\n] [-ctmo nb_cycle]\n");
  exit(EXIT_FAILURE);
}

void	error_dump(int mode)
{
  if (mode == 0)
    my_printf("Dump number must be a positive number\n");
  if (mode == 1)
    my_printf("CTMO number must be a positive number\n");
  if (mode == 2)
    my_printf("Please give only numbers for -n and -a\n");
  exit(EXIT_FAILURE);
}

void	error_champ(int mode, char *s1, char *s2)
{
  if (mode == 0)
    my_printf("VM needs between 1 and 4 champion(s) for work !\n");
  if (mode == 1)
    my_printf("The champion must be a .cor\n");
  if (mode == 2)
    my_printf("The VM is configure for 4 champions only\n");
  if (mode == 3)
    my_printf("Binaire corrompu: %s\n", s1);
  if (mode == 4)
    my_printf("Incorrect path for : %s\n", s1);
  exit(EXIT_FAILURE);
}
