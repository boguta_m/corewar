/*
** mem.h for mem.h in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Mar 25 22:01:20 2014 camill_n
** Last update Sat Apr 12 16:25:53 2014 camill_n
*/

#ifndef MEM_H_
# define MEM_H_

int	get_param(unsigned char param, int mode);
int	get_all_param(t_data *data, int *type, int *arg, t_process *proc);
int	get_ind_param(t_data *data, int *type, int *arg, t_process *proc);
int	show_param(unsigned char param);
int	get_val(t_data *data, int type, int pos, t_process *proc);
int	set_val(t_data *data, int pos, int src, int size);
int	get_jump(t_data *data, unsigned char param, t_process *proc, int size);
int	char_to_int(t_data *data, int *val, int pos, int size);

#endif
