/*
** load.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Mar  2 17:08:12 2014 camill_n
** Last up Sun Mar  2 17:12:21 2014 camill_n
*/

#ifndef LOAD_H_
# define LOAD_H_

typedef struct	s_process t_proc;

int	ld(t_data *data, t_process *proc);
int	lld(t_data *data, t_process *proc);
int	ldi(t_data *data, t_process *proc);
int	st(t_data *data, t_process *proc);
int	sti(t_data *data, t_process *proc);

#endif
