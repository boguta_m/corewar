/*
** print.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Mar  2 17:08:12 2014 camill_n
** Last up Sun Mar  2 17:12:21 2014 camill_n
*/

#ifndef UTILS_H_
# define UTILS_H_

typedef struct	s_prog t_prog;
typedef struct	s_process t_process;

int	print_result(t_data *data);
int	print_process(t_process *proc);
int	print_mem(unsigned char *mem, int size);
int	my_bzero(char *str, int i);
int	my_ubzero(unsigned char *str, int i);

#endif
