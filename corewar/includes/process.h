/*
** process.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Mar  2 17:08:12 2014 camill_n
** Last up Sun Mar  2 17:12:21 2014 camill_n
*/

#ifndef PROCESS_H_
# define PROCESS_H_

# define PROC		data->proc
# define PROC_ID	data->proc->champ_id
# define PROC_NEXT_INS	data->proc->next_ins

typedef struct		s_process
{
  int			champ_id;
  int			pc;
  int			time_to_exec;
  char			carry;
  int			reg[REG_NUMBER];
  struct s_process	*next;
}			t_process;

int	add_proc(t_process **proc, int champ_id, int pos, int *reg);
int	set_carry(t_process **proc, int carry);

#endif
