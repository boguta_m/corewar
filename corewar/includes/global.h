/*
** global.h for my_select in /home/camill_n/rendu/PSU_2013_my_select
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Sat Jan 11 00:01:31 2014 Nicolas Camilli
** Last update Sun Apr 13 14:51:59 2014 camill_n
*/

#ifndef GLOBAL_H_
# define GLOBAL_H_

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <SDL/SDL.h>
# include "func.h"
# include "alloc.h"
# include "op.h"
# include "complement.h"
# include "errors.h"
# include "init.h"
# include "get_next_line.h"
# include "wordtab.h"
# include "argv.h"
# include "error_arg.h"
# include "vm.h"
# include "utils.h"
# include "process.h"
# include "logic.h"
# include "arithmetic.h"
# include "simple.h"
# include "live.h"
# include "mem.h"
# include "load.h"

#endif
