/*
** init.h for init in /home/camill_n/rendu/MUL_2013_rtv1
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Wed Feb  5 16:29:48 2014 Nicolas Camilli
** Last update Sun Apr 13 14:40:09 2014 camill_n
*/

#ifndef INIT_H_
# define INIT_H_

typedef struct s_data	t_data;

int	load_champ(t_data *data, char *path[]);
int	load_vm_func(t_data *data);
int	make_bin(t_data *data, int i, char *path[]);
int	verif_number(t_data *data);
int	verif_ctmo(t_data *data, int val);

#endif
