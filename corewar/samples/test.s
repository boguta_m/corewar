.name "test"
.comment "Jour J"

	#sti r1, %:live, %1
	fork %:fork

	and r2, %0, r2
live:	live %999
	zjmp %:live

fork:	and r2, %0, r2
	live %999
	#sti r1, %:live, %1
	fork %:fork
	zjmp %:fork
