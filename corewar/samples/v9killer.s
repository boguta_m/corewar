.name "v9Killer"
.comment "Hey v9 ! I kill you ! Thanks for the coffees"

	sti r1, %:live, %1
	and r2, %0, r2
init:	fork %:live
	fork %:begin
	fork %:init
	fork %:begin
	fork %:init
	fork %:begin
	fork %:init
	fork %:begin
	zjmp %:init

begin:	lfork %4080
	lfork %4280
	lfork %4380
	lfork %4480
	zjmp %:init

live:	live %1
	zjmp %:live
