.name "EraserV2"
.comment "I want to erase you !"

haha:	sti r1, %:live, %1
	ld %57672193, r2 # (st r2, X)
	fork %:eraser
	sti r1, %:haha, %0
	st r1, 6
	live %1
	and r16, %0, r16
	fork %:live
	and r2, %0, r2
	zjmp %:init

live:	live %1
	zjmp %:live

init:	sti r1, %:live_d, %1
	ld %4, r3
	ld %200, r4 # (200)
	ld %0, r6
	ld %8, r7

fork1:	fork %:fork2
	add r6, r7, r6

fork2:	fork %:inidup
	add r6, r3, r6

inidup:	ldi r6, %:dup, r5
	and r16, %0, r16
dup:	sti r5, r4, r6
live_d:	live %1
	zjmp %190 # (r4 - 10)

eraser:	st r2, 461
	and r16, %0, r16
	zjmp %448 # (-13)

end:
