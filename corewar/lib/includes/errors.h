/*
** errors.h for my_select in /home/camill_n/rendu/PSU_2013_my_select
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Sun Jan  5 16:02:32 2014 Nicolas Camilli
** Last update Sun Mar  2 17:43:38 2014 camill_n
*/

#ifndef ERRORS_H_
# define ERRORS_H_

void	error_malloc(char *name_var);
void	error_file_path(char *name);

#endif
