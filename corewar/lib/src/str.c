/*
** str.c for str in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Feb 11 14:41:54 2014 camill_n
** Last update Sun Mar  2 17:52:11 2014 camill_n
*/

#include "global.h"

char	*my_strdup(char *str)
{
  char	*tmp;
  int	i;

  tmp = x_malloc((my_strlen(str) + 1) * sizeof(char), "tmp");
  i = 0;
  while (str[i] != '\0')
    {
      tmp[i] = str[i];
      ++i;
    }
  tmp[i] = '\0';
  return (tmp);
}

int	is_number(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] < '0' || str[i] > '9')
	return (-1);
      ++i;
    }
  return (0);
}
