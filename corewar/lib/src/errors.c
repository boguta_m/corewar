/*
** errors.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Feb 10 20:51:32 2014 camill_n
** Last update Sun Mar  2 17:43:52 2014 camill_n
*/

#include "global.h"

void	error_malloc(char *str)
{
  my_printf("Memory access denied for var: %s\n", str);
  exit(0);
}

void	error_file_path(char *name)
{
  my_printf("Error, the file %s is not accessible\n", name);
  exit(0);
}
