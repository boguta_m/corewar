/*
** test.c for my_putnbr in /home/camill_n/Documents/testnbr
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Fri Oct  4 20:37:32 2013 Nicolas Camilli
** Last update Sun Nov 17 10:20:15 2013 Nicolas Camilli
*/

#include "../includes/func.h"

void	manage_put_nbr(va_list ap)
{
  int	tmp;

  tmp = va_arg(ap, int);
  if (tmp == 0)
    my_putchar('0');
  else
    my_put_nbr(tmp);
}

void		manage_put_long(va_list ap)
{
  long long	tmp;

  tmp = va_arg(ap, long);
  if (tmp == 0)
    my_putchar('0');
  else
    my_put_long(tmp);
}

void	my_put_nbr(int nb)
{
  int	tmp;

  if (nb < 0)
    {
      my_putchar('-');
      nb = nb * -1;
    }
  if (nb > 0)
    {
      tmp = nb % 10;
      nb = nb / 10;
      my_put_nbr(nb);
      my_putchar(tmp + 48);
    }
}

void	my_put_long(long nb)
{
  long	tmp;

  if (nb < 0)
    {
      my_putchar('-');
      nb = nb * -1;
    }
  if (nb > 0)
    {
      tmp = nb % 10;
      nb = nb / 10;
      my_put_nbr(nb);
      my_putchar(tmp + 48);
    }
}
