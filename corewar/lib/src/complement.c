/*
** complement.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Feb 10 20:51:11 2014 camill_n
** Last update Sun Apr 13 19:52:54 2014 Maxime Boguta
*/

#include "global.h"

char	*my_strcat(char *s1, char *s2)
{
  char	*new;
  int	i;
  int	j;

  i = 0;
  j = 0;
  new = x_malloc(my_strlen(s1) + my_strlen(s2) + 1 ,"new");
  while (s1[i] != '\0')
    new[i++] = s1[j++];
  j = 0;
  while (s2[j] != '\0')
    new[i++] = s2[j++];
  new[i] = '\0';
  return (new);
}

int	is_alnum(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] < 48 && str[i] != ' ' && str[i] != '#' && str[i] != '/' &&
	  str[i] != '.')
	return (0);
      if (str[i] > 57 && str[i] < 65)
	return (0);
      if (str[i] > 90 && str[i] < 97)
	return (0);
      if (str[i] > 127)
	return (0);
      ++i;
    }
  return (1);
}

int	my_strncmp(char *s1, char *s2, int i, int j)
{
  if (i - j == 0)
    return (1);
  while (i < j && s1[i] != '\0' && s2[i] != '\0')
    {
      if (s1[i] != s2[i])
        return (s1[i] - s2[i]);
      ++i;
    }
  if ((s1[i] == '\0' && s2[i] == '\0') || i == j)
    return (0);
  if (s1[i] == '\0')
    return (0 - s2[i]);
  else
    return (s1[i]);
}

int	check_last(char *check, char *str)
{
  int	nb;
  int	size;
  int	i;
  int	k;

  k = 0;
  nb = my_strlen(check);
  size = my_strlen(str);
  i = size - nb;
  if (size < my_strlen(check))
    return (-1);
  while (i < size + 1)
    {
      if (check[k] != str[i])
	return (-1);
      ++i;
      ++k;
    }
  return (0);
}
