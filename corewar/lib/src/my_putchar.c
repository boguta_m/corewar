/*
** my_putchar.c for my_putchar in /home/camill_n/rendu/Piscine-C-lib/my
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Tue Oct  8 07:18:20 2013 Nicolas Camilli
** Last update Thu Apr  3 01:19:09 2014 camill_n
*/

#include <unistd.h>
#include "../includes/func.h"

void	manage_putchar(va_list ap)
{
  my_putchar((char)va_arg(ap, int));
}

int		my_putchar(char c)
{
  unsigned char	c2;

  c2 = (unsigned char)c;
  write(1, &c2, 1);
  return (0);
}
