##README##

Some samples can be found in corewar/samples. Take a look at the syntax.

In order to compile an assembler source file, give the "asm" binary a .s file.

You can use the user-friendly launcher in the "bonus" folder for compiling or launching a game.

Up to 4 champions can be loaded into the VM.

Options :
CYCLE TO DIE : Each champion has to call a "live" instruction each CYCLE TO DIE cycles in order to stay alive in the arena.
NUM : Specific number given to a champion.
ADDRESS : Memory starting place for the loaded champion.

EPITECH 2014 - Maxime Boguta - Nicolas Camilli - Antonin Bouscarel
