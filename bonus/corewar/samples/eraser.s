.name "Eraser"
.comment "I want to erase you !"

haha:	sti r1, %:l0, %1
	ld %57672193, r2 # (st r2, 256)
	fork %:eraser
	st r1, 6
	live %1
	st r1, 6
	live %1
	sti r1, %:l1, %1
	sti r1, %:l2, %1
	and r2, %0, r2
	ld %655359, r14 #(zjmp -1)
	sti r14, %:haha, %-5
	fork %:init

l0:	live %1
l1:	live %1
l2:	live %2
	zjmp %:l0

init:	sti r1, %:live_d, %1
	ld %4, r3
	ld %52, r4 # Nombres d'octets à dupliquer depuis begin
	ld %460, r6 # A partir de combien d'octets (210)

begin:	ld %0, r2
b_dup:	ldi r2, %:begin, r5
	sti r5, r6, r2
	add r2, r3, r2
	sub r2, r4, r16
	zjmp %445 # (r6 - 15)
live_d:	live %1
	and r15, %0, r15
	zjmp %:b_dup

end:	aff r16

eraser:	st r2, 441
	zjmp %440

end:
