.name "ForkyV3"
.comment "I am a fork program"

	ld %4, r3
	ld %250, r4
	ld %8, r6
	ld %12, r7
	ld %150994944, r8
	ld %0, r2
init:	sti r1, %:live_d, %1
	sti r8, %:init, %0
	fork %:fork1
	sti r1, %:live, %1
	ld %0, r2

live:	live %1
	zjmp %:live

fork1:	fork %:fork2
	add r2, r7, r2

fork2:	fork %:fork3
	add r2, r6, r2

fork3:	fork %:dup
	add r2, r3, r2

dup:	ldi r2, %:dup2, r5
	ld %0, r16
	fork %:live_d
	zjmp %:fork1

dup2:	zjmp %0
live_d:	live %1
	sti r5, r4, r2
	zjmp %248 # (r4 - 2)

# r2 : Index de l'octet à copier pour ce fork
# r3 : Contient 4
# r4 : Position où on copie
# r5 : 4 octets à r2
