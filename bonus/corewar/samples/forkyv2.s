.name "ForkyV2"
.comment "I am a fork program"

init:	sti r1, %:live, %1
	sti r1, %:live_d, %1
	fork %:live
	ld %4, r3
	ld %52, r4 # Nombres d'octets à dupliquer depuis begin
	ld %460, r6
	and r2, %0, r2
	zjmp %:begin

live:	live %1
	zjmp %:live

begin: 	ld %0, r2
b_dup:	ldi r2, %:begin, r5
	sti r5, r6, r2
	add r2, r3, r2
	sub r2, r4, r16
	zjmp %445 # r6 - 4 * 5
live_d:	live %1
	and r15, %0, r15
	zjmp %:b_dup # (r2 != r4)

end:	aff r16
