/*
** my_strcmp.c for my_strcmp in /home/camill_n/rendu/Piscine-C-Jour_06/ex_05
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Mon Oct  7 14:19:44 2013 Nicolas Camilli
** Last update Tue Jan 14 15:23:51 2014 Nicolas Camilli
*/

#include <stdio.h>

int	 my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s1[i] != '\0' && s2[i] != '\0')
    {
      if (s1[i] != s2[i])
	return (s1[i] - s2[i]);
      ++i;
    }
  if (s1[i] == '\0' && s2[i] == '\0')
    return (0);
  if (s1[i] == '\0')
    return (0 - s2[i]);
  else
    return (s1[i]);
}
