/*
** argv.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Mar  2 17:08:12 2014 camill_n
** Last up Sun Mar  2 17:12:03 2014 camill_n
*/

#include "global.h"

int	simple_prog(int *prog, char **av, int *i, int err_code)
{
  if (av[*i + 1] == NULL || is_number(av[*i + 1]) == -1)
    error_dump(err_code);
  *prog = my_getnbr(av[*i + 1]);
  *i += 1;
  return (0);
}

int	*init_prog(t_data *data)
{
  DUMP = -1;
  CTMO = -1;
  NB_PROG = 0;
  PROG_NUMBER[0] = 1;
  PROG_NUMBER[1] = 2;
  PROG_NUMBER[2] = 3;
  PROG_NUMBER[3] = 4;
  PROG_ADDR[0] = -1;
  PROG_ADDR[1] = -1;
  PROG_ADDR[2] = -1;
  PROG_ADDR[3] = -1;
  return (0);
}

int	add_champ(t_data *data, char **av, int *i, char *path[])
{
  int	tmp;

  if ((*i > 4 && av[*i - 4] != NULL && my_strcmp(av[*i - 4], FLAG_NB) == 0) ||
      (*i > 4 && av[*i - 4] != NULL && my_strcmp(av[*i - 4], FLAG_ADDR) == 0))
    {
      tmp = *i - 4;
      if (my_strcmp(av[*i - 4], FLAG_ADDR) == 0)
	simple_prog(&(PROG_ADDR[NB_PROG]), av, &tmp, 2);
      else
	simple_prog(&(PROG_NUMBER[NB_PROG]), av, &tmp, 2);
    }
  if ((*i > 2 && av[*i - 2] != NULL && my_strcmp(av[*i - 2], FLAG_NB) == 0) ||
      (*i > 2 && av[*i - 2] != NULL && my_strcmp(av[*i - 2], FLAG_ADDR) == 0))
    {
      tmp = *i - 2;
      if (my_strcmp(av[*i - 2], FLAG_ADDR) == 0)
	simple_prog(&(PROG_ADDR[NB_PROG]), av, &tmp, 2);
      else
	simple_prog(&(PROG_NUMBER[NB_PROG]), av, &tmp, 2);
    }
  if (check_last(EXT_BIN, av[*i]) != 0)
    error_champ(1, NULL, NULL);
  path[NB_PROG] = my_strdup(av[*i]);
  ++NB_PROG;
  return (0);
}

int	load(t_data *data, int ac, char **av, char *path[])
{
  int	i;

  i = 1;
  init_prog(data);
  while (i < ac)
    {
      if (av[i][0] != '-')
	add_champ(data, av, &i, path);
      if (my_strcmp(av[i], FLAG_DUMP) == 0 && DUMP == -1)
	simple_prog(&DUMP, av, &i, 0);
      if (my_strcmp(av[i], FLAG_BLOC) == 0 && CTMO == -1)
	simple_prog(&CTMO, av, &i, 1);
      av[i][0] == '-' ? ++i : 0;
      ++i;
    }
  (DUMP < 1) ? error_dump(0) : 0;
  (NB_PROG < 1) ? error_champ(0, NULL, NULL) : 0;
  (NB_PROG > MAX_ARGS_NUMBER) ? error_champ(2, NULL, NULL) : 0;
  return (0);
}
