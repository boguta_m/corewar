/*
** load.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu Apr  3 00:04:04 2014 camill_n
** Last update Sun Apr 13 13:49:05 2014 camill_n
*/

#include "global.h"

int	sti(t_data *data, t_process *proc)
{
  int	reg;
  int	dst;
  int	jump;
  int	i;

  i = -1;
  reg = get_val(data, IND_REG_VAL, POS + 2, proc);
  jump = 3;
  dst = 0;
  while (++i < 2)
    {
      if (get_param(VM->map[POS + 1], 1 + i) == REG_VAL &&
	  VM->map[(POS + jump) % MEM_SIZE] >= 1 &&
	  VM->map[(POS + jump) % MEM_SIZE] <= 16)
	dst += REG[VM->map[(POS + jump) % MEM_SIZE] - 1];
      else
	dst += get_val(data, IND_DIR_VAL, POS + jump, proc);
      get_param(VM->map[POS + 1], 1 + i) != REG_VAL ? ++jump : 0;
      ++jump;
    }
  VM->ctmo > 0 ? dst %= IDX_MOD : 0;
  if (reg >= 1 && reg <= REG_NUMBER)
    set_val(data, POS + dst, REG[--reg], REG_SIZE);
  proc->pc += jump;
  return (0);
}

int	ldi(t_data *data, t_process *proc)
{
  int	type[4];
  int	val[4];
  int	jump;
  int	dst;

  get_ind_param(data, type, val, proc);
  dst = val[0] + val[1];
  jump = 6;
  type[0] == REG_VAL ? --jump : 0;
  type[1] == REG_VAL ? --jump : 0;
  val[2] = (int)VM->map[(POS + jump) % MEM_SIZE];
  if (val[2] >= 1 && val[2] <= REG_NUMBER)
    {
      REG[val[2] - 1] =
	get_val(data, DIRECT_VAL, POS + (dst % IDX_MOD), proc);
      proc->carry = -1;
      if (get_val(data, DIRECT_VAL, POS + (dst % IDX_MOD), proc) == 0)
	proc->carry = 1;
    }
  proc->pc += (jump + 1);
  return (0);
}

int	ld(t_data *data, t_process *proc)
{
  int	type[4];
  int	val[4];

  get_all_param(data, type, val, proc);
  if (val[1] <= REG_NUMBER && val[1] >= 1)
    REG[val[1] - 1] = val[0];
  proc->carry = (val[0] == 0) ? 1 : 0;
  proc->pc += get_jump(data, VM->map[POS + 1], proc, 3);
  return (0);
}

int	lld(t_data *data, t_process *proc)
{
  int	type[4];
  int	val[4];

  get_all_param(data, type, val, proc);
  if (val[1] <= REG_NUMBER && val[1] >= 1)
    REG[val[1] - 1] = val[0];
  proc->carry = (val[0] == 0) ? 1 : 0;
  proc->pc += get_jump(data, VM->map[POS + 1], proc, 3);
  return (0);
}

int	st(t_data *data, t_process *proc)
{
  int	type[4];
  int	val[4];

  get_all_param(data, type, val, proc);
  if (type[1] == INDIRECT_VAL)
    val[1] = get_val(data, IND_DIR_VAL, POS + 3, proc);
  if (val[0] <= 16 && val[0] >= 1)
    {
      if (type[1] == REG_VAL && val[1] >= 1 && val[1] <= REG_NUMBER)
	REG[val[1] - 1] = REG[val[0] - 1];
      else
	set_val(data, POS + (val[1] % IDX_MOD), REG[val[0] - 1], 4);
    }
  proc->pc += get_jump(data, VM->map[POS + 1], proc, 3);
  return (0);
}
