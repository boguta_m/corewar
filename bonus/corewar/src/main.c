/*
** main.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Feb 10 20:50:26 2014 camill_n
** Last update Wed Mar 26 13:00:06 2014 camill_n
*/

#include "global.h"

int		main(int ac, char **av)
{
  t_data	data;
  t_vm		vm;
  char		*path[4];

  ac < 3 ? error_arg(*av) : 0;
  data.vm = &vm;
  load(&data, ac, av, path);
  load_vm_func(&data);
  load_champ(&data, path);
  print_result(&data);
  print_process(data.proc);
  exec(&data);
  return (0);
}
