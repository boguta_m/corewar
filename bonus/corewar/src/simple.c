/*
** simple.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Mar  2 17:08:12 2014 camill_n
** Last up Sun Mar  2 17:12:03 2014 camill_n
*/

#include "global.h"

int	zjmp(t_data *data, t_process *proc)
{
  int	jump;

  jump = (char)get_val(data, IND_DIR_VAL, POS + 1, proc);
  if (proc->carry == 1)
    proc->pc += jump;
  else
    proc->pc += 3;
  return (0);
}

int	vm_lfork(t_data *data, t_process *proc)
{
  int	addr;

  addr = get_val(data, IND_DIR_VAL, POS + 1, proc);
  add_proc(&PROC, proc->champ_id, POS + addr, proc->reg);
  proc->pc += 3;
  ++VM->nb_proc;
  return (0);
}

int	vm_fork(t_data *data, t_process *proc)
{
  int	addr;

  addr = get_val(data, IND_DIR_VAL, POS + 1, proc);
  add_proc(&PROC, proc->champ_id, POS + (addr % IDX_MOD), proc->reg);
  proc->pc += 3;
  ++VM->nb_proc;
  return (0);
}

int	aff(t_data *data, t_process *proc)
{
  int	reg;

  reg = get_val(data, IND_REG_VAL, POS + 2, proc);
  if (reg >= 1 && reg <= REG_NUMBER)
    my_printf("%c\n", REG[reg - 1] % 256);
  proc->pc += 3;
  return (0);
}

int	lldi(t_data *data, t_process *proc)
{
  int	type[4];
  int	val[4];
  int	jump;
  int	dst;

  get_ind_param(data, type, val, proc);
  dst = val[0] + val[1];
  jump = 6;
  type[0] == REG_VAL ? --jump : 0;
  type[1] == REG_VAL ? --jump : 0;
  val[2] = (int)VM->map[(POS + jump) % MEM_SIZE];
  if (val[2] >= 1 && val[2] <= REG_NUMBER)
    {
      REG[val[2] - 1] =
	get_val(data, DIRECT_VAL, POS + dst, proc);
      proc->carry = -1;
      if (get_val(data, DIRECT_VAL, POS + dst, proc) == 0)
	proc->carry = 1;
    }
  proc->pc += (jump + 1);
  return (0);
}
