/*
** init_disp.c for corewar in /home/camill_n/rendu/CPE_2014_corewar/bonus
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Apr 13 15:33:27 2014 camill_n
** Last update Sun Apr 13 15:45:41 2014 camill_n
*/

#include "global.h"

int		set_pos(t_display *disp)
{
  disp->position.x = (640/2) - (600/2);
  disp->position.y = (480/2) - (450/2);
  disp->postext.x = 20;
  disp->postext.y = 600;
  disp->pos_footer.x = 0;
  disp->pos_footer.y = 600;
  return (0);
}

int		init_pc(t_display *disp, int i, int r, int g, int b)
{
  disp->pc[i] = SDL_CreateRGBSurface(SDL_HWSURFACE, 10, 10, 32, 0, 0, 0, 0);
  SDL_FillRect(disp->pc[i],
	       NULL, SDL_MapRGB(disp->pc[i]->format, r, g, b));

  return (0);
}

int		init_p(t_display *disp, int i, int r, int g, int b)
{
  disp->player[i] = SDL_CreateRGBSurface(SDL_HWSURFACE, 10, 10, 32, 0, 0, 0, 0);
  SDL_FillRect(disp->player[i],
	       NULL, SDL_MapRGB(disp->player[i]->format, r, g, b));
  return (0);
}

t_display	*init_vid()
{
  t_display	*disp;
  SDL_Color	noir = {0, 0, 0};

  disp = x_malloc(sizeof(*disp), "disp");
  SDL_Init(SDL_INIT_VIDEO);
  disp->ecran = SDL_SetVideoMode(1500, 900, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
  if (disp->ecran == NULL)
    exit (EXIT_FAILURE);
  SDL_WM_SetCaption("Corewar", NULL);
  SDL_FillRect(disp->ecran, NULL, SDL_MapRGB(disp->ecran->format, 55, 54, 58));
  TTF_Init();
  disp->footer = SDL_CreateRGBSurface(SDL_HWSURFACE, 1000, 50, 32, 0, 0, 0, 0);
  SDL_FillRect(disp->footer,
	       NULL, SDL_MapRGB(disp->footer->format, 255, 255, 255));
  set_pos(disp);
  init_p(disp, 0, 0, 0, 255);
  init_p(disp, 1, 0, 255, 0);
  init_p(disp, 2, 255, 0, 0);
  init_p(disp, 3, 255, 255, 0);
  init_p(disp, 4, 255, 255, 255);
  init_pc(disp, 0, 0, 0, 120);
  init_pc(disp, 1, 0, 120, 0);
  init_pc(disp, 2, 120, 0, 0);
  init_pc(disp, 3, 120, 120, 0);
  disp->police = TTF_OpenFont("arial.ttf", 20);
  disp->color = noir;
  return (disp);
}
