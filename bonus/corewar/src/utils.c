/*
** print.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Mar  2 17:08:12 2014 camill_n
** Last up Sun Mar  2 17:12:03 2014 camill_n
*/

#include "global.h"

int		print_result(t_data *data)
{
  int		i;

  i = 0;
  my_printf("\n=================== Corewar - VM ===================\n\n");
  my_printf(" -> Nombre de cycles: %d\n", DUMP);
  if (CTMO == -1)
    my_printf(" -> CTMO non active.\n");
  else
    my_printf(" -> CTMO : %d\n", CTMO);
  my_printf(" -> Nombre de champions dans l'arène: %d\n\n", NB_PROG);
  while (i < NB_PROG)
    {
      my_printf("  -> Champion %d:\n", i);
      my_printf("     -> Nom: %s\n", HEADER[i].prog_name);
      my_printf("     -> Comment: %s\n", HEADER[i].comment);
      my_printf("     -> Numero: %d\n", PROG_NUMBER[i]);
      my_printf("     -> Addr prog: %d\n\n", PROG_ADDR[i]);
      ++i;
    }
  return (0);
}

int		print_process(t_process *proc)
{
  t_process	*tmp;

  tmp = proc;
  while (tmp != NULL)
    {
      my_printf("Processus du champion numero: %d\n", tmp->champ_id);
      my_printf("Processus a la position: %d\n\n", tmp->pc);
      tmp = tmp->next;
    }
  return (0);
}

int		print_mem(unsigned char *mem, int size)
{
  int		i;

  i = 0;
  while (i < size)
    {
      if (i % 32 == 0)
	my_putchar('\n');
      if (mem[i] == 0)
	my_printf("00", mem[i]);
      else if (mem[i] > 0xf)
	my_printf("%X", mem[i]);
      else
	my_printf("0%X", mem[i]);
      ++i;
    }
  my_putchar('\n');
  return (0);
}

int		my_bzero(char *str, int i)
{
  int		k;

  k = 0;
  while (k < i)
    {
      str[k] = 0;
      ++k;
    }
  return (0);
}

int		my_ubzero(unsigned char *str, int i)
{
  int		k;

  k = 0;
  while (k < i)
    {
      str[k] = 0;
      ++k;
    }
  return (0);
}
