/*
** logic.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat Mar 22 14:47:41 2014 camill_n
** Last update Sun Apr 13 14:29:57 2014 camill_n
*/

#include "global.h"

int	and(t_data *data, t_process *proc)
{
  int	type[4];
  int	val[4];
  int	ret;

  get_all_param(data, type, val, proc);
  if (val[2] <= REG_NUMBER && val[2] >= 1)
    {
      ret = val[0] & val[1];
      REG[val[2] - 1] = ret;
      proc->carry = (ret == 0 ? 1 : -1);
    }
  proc->pc += get_jump(data, VM->map[(POS + 1) % MEM_SIZE], proc, 3);
  return (0);
}

int	or(t_data *data, t_process *proc)
{
  int	type[4];
  int	val[4];
  int	ret;

  get_all_param(data, type, val, proc);
  if (val[2] <= REG_NUMBER && val[2] >= 1)
    {
      ret = val[0] | val[1];
      REG[val[2] - 1] = ret;
      proc->carry = (ret == 0 ? 1 : -1);
    }
  proc->pc += get_jump(data, VM->map[(POS + 1) % MEM_SIZE], proc, 3);
  return (0);
}

int	xxor(t_data *data, t_process *proc)
{
  int	type[4];
  int	val[4];
  int	ret;

  get_all_param(data, type, val, proc);
  if (val[2] <= REG_NUMBER && val[2] >= 1)
    {
      ret = val[0] ^ val[1];
      REG[val[2] - 1] = ret;
      proc->carry = (ret == 0 ? 1 : -1);
    }
  proc->pc += get_jump(data, VM->map[(POS + 1) % MEM_SIZE], proc, 3);
  return (0);
}
