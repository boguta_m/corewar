/*
** init2.c for corewar in /home/camill_n/rendu/core/CPE_2014_corewar/corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat Apr 12 11:50:43 2014 camill_n
** Last update Sun Apr 13 15:59:57 2014 camill_n
*/

#include "global.h"

int		rev_endian(int nb)
{
  int		new;

  new = 0;
  new += (nb & 0xff000000) >> 24;
  new += (nb & 0x000000ff) << 24;
  new += (nb & 0x0000ff00) << 8;
  new += (nb & 0x00ff0000) >> 8;
  return (new);
}

int		get_header(t_data *data, int fd, int i, char *path[])
{
  int		ret;

  ret = read(fd, &(HEADER[i]), sizeof(header_t));
  if (ret < 1)
    error_champ(4, "", path[i]);
  HEADER[i].prog_size = rev_endian(HEADER[i].prog_size);
  return (0);
}

int		make_bin(t_data *data, int i, char *path[])
{
  int		ret;
  unsigned char	*buff;
  int		fd;
  int		j;
  int		rank;

  fd = open(path[i], O_RDONLY);
  fd < 0 ? error_champ(4, path[i], NULL) : 0;
  get_header(data, fd, i, path);
  buff = x_malloc(HEADER[i].prog_size * sizeof(unsigned char), "buff");
  rank = PROG_ADDR[i];
  while ((ret = read(fd, buff, HEADER[i].prog_size)) > 0)
    {
      ret != HEADER[i].prog_size ? error_champ(3, path[i], NULL) : 0;
      j = 0;
      while (rank < MEM_SIZE && j < HEADER[i].prog_size)
	{
	  VM->map_champ[rank] = i + 1;
	  VM->map[rank++] = buff[j++];
	}
    }
  free(buff);
  close(fd);
  return (0);
}

int		verif_number(t_data *data)
{
  int		i;
  int		j;

  i = 0;
  while (i < NB_PROG)
    {
      j = 0;
      if (PROG_NUMBER[i] > NB_PROG || PROG_NUMBER[i] <= 0)
	{
	  my_printf("Prog number can't be %d, there is only %d champion(s) in arena\n",
		    PROG_NUMBER[i], NB_PROG);
	  exit(0);
	}
      while (j < NB_PROG)
	{
	  if (PROG_NUMBER[i] == PROG_NUMBER[j] && i != j)
	    {
	      my_printf("The champion %d and %d have the same number.\n", ++i, ++j);
	      exit(0);
	    }
	  ++j;
	}
      ++i;
    }
  return (0);
}

int		verif_ctmo(t_data *data, int val)
{
  if (VM->ctmo > 0)
    {
      if (val == 0xd || val == 0xe || val == 0xf)
	return (-1);
    }
  return (0);
}
