#include <stdio.h>
#include "global.h"

char		*convert(int nb)
{
  char		*buff;
  int		i;

  i = 0;
  buff = x_malloc(sizeof(char) * 15, "buff");
  while (nb > 0)
    {
      buff[i] = (nb % 10) + 48;
      nb = nb / 10;
      ++i;
    }
  if (i == 0)
    {
      buff[i] = '0';
      ++i;
    }
  buff[i] = 0;
  return (my_revstr(buff));
}


int		verif_pc(t_data *data, int *id, int i)
{
  t_process	*proc;

  proc = data->proc;
  while (proc != NULL)
    {
      if (proc->pc == i)
	{
	  *id = proc->champ_id;
	  return (0);
	}
      proc = proc->next;
    }
  return (-1);
}

int		display(t_data *data, t_display *disp, int dump)
{
  int		i;
  int		j;
  int		k;
  SDL_Surface	*texte;
  char		*line;
  int		id;
  j = 0;
  k = 0;
  line = my_strcat("cycle : ", convert(dump));
  line = my_strcat(line, "                      ");
  line = my_strcat(line, "nb_live : ");
  line = my_strcat(line, convert(VM->nb_live));
  line = my_strcat(line, "                      ");
  line = my_strcat(line, "nb_proc : ");
  line = my_strcat(line, convert(VM->nb_proc));
  while (j < GRILLE_HAU)
    {
      i = 0;
      while (i < GRILLE_LAR)
        {
	  if (verif_pc(data, &id, k) == -1)
	    {
	      if (data->vm->map[k] != 0 || data->vm->map_champ[k] != -1)
		disp->grille[j][i] = disp->player[data->vm->map_champ[k] - 1];
	      else
		disp->grille[j][i] = disp->player[4];
	    }
	  else
	    disp->grille[j][i] = disp->pc[id];
	  disp->position.x= 2 + i * 12;
	  disp->position.y= 2 + j * 12;
	  SDL_BlitSurface(disp->grille[j][i], NULL, disp->ecran, &disp->position);
	  ++k;
	  i++;
        }
      j++;
    }
  if (dump % 10 == 0)
    {
      SDL_BlitSurface(disp->footer, NULL, disp->ecran, &disp->pos_footer);
      texte = TTF_RenderText_Blended(disp->police, line, disp->color);
      SDL_BlitSurface(texte, NULL, disp->ecran, &disp->postext);
    }
  SDL_Flip(disp->ecran);
  return EXIT_SUCCESS;
}
