/*
** vm.h for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Mar  2 17:15:36 2014 camill_n
** Last update Sun Apr 13 14:44:52 2014 camill_n
*/

#ifndef VM_H_
# define VM_H_

# define VM			data->vm
# define HEADER			data->header
# define COMMENT		HEADER->prog_comment
# define NB_PROG		VM->nb_prog
# define PROG_NUMBER		VM->prog_number
# define PROG_ADDR		VM->prog_addr
# define PROG_LIVE		VM->prog_live
# define DUMP			VM->dump
# define CTMO			VM->ctmo
# define NB_INS			16
# define VM_FUNC		data->vm_func
# define REG			proc->reg
# define CYCLETODIE		VM->cycle_to_die
# define NB_LIVE		VM->nb_live
# define STATE_LAST_LIVE	VM->state_last_live
# define CYCLE_BEFORE_DIE	VM->cycle_before_die
# define REG_VAL		1
# define DIRECT_VAL		2
# define INDIRECT_VAL		3
# define IND_DIR_VAL		4
# define IND_REG_VAL		5
# define IND_REG_SIZE		1
# define VAL_POS		VM->map[proc->pc]
# define POS			proc->pc

typedef struct	s_flag t_flag;
typedef struct	s_process t_process;

typedef struct	s_vm
{
  int		dump;
  int		nb_live;
  int		state_last_live;
  int		ctmo;
  int		nb_prog;
  int		cycle_to_die;
  int		cycle_before_die;
  unsigned char	map[MEM_SIZE];
  int		map_champ[MEM_SIZE];
  int		nb_proc;
  int		prog_live[MAX_ARGS_NUMBER];
  int		prog_number[MAX_ARGS_NUMBER];
  int		prog_addr[MAX_ARGS_NUMBER];
}		t_vm;

typedef struct	s_vm_func
{
  unsigned char	val;
  int		time;
  int		(*execute)(t_data *data, t_process *proc);
}		t_vm_func;

typedef struct	s_data
{
  t_process	*proc;
  header_t	header[MAX_ARGS_NUMBER];
  t_vm		*vm;
  t_vm_func	vm_func[NB_INS];
  int		current_proc;
}		t_data;

int		exec(t_data *data);

#endif
