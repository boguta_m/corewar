/*
** simple.h for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat Mar 22 14:46:02 2014 camill_n
** Last update Thu Apr  3 01:04:42 2014 camill_n
*/

#ifndef SIMPLE_H_
# define SIMPLE_H_

int	zjmp(t_data *data, t_process *proc);
int	vm_fork(t_data *data, t_process *proc);
int	vm_lfork(t_data *data, t_process *proc);
int	aff(t_data *data, t_process *proc);
int	lldi(t_data *data, t_process *proc);

#endif
