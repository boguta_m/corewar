/*
** arithmetic.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Mar  2 17:08:12 2014 camill_n
** Last up Sun Mar  2 17:12:21 2014 camill_n
*/

#ifndef ARITHMETIC_H_
# define ARITHMETIC_H_

int	add(t_data *data, t_process *proc);
int	sub(t_data *data, t_process *proc);

#endif
