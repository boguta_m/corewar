/*
** argv.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Mar  2 17:08:12 2014 camill_n
** Last up Sun Mar  2 17:12:21 2014 camill_n
*/

#ifndef ARGV_H_
# define ARGV_H_

# define FLAG_DUMP "-dump"
# define FLAG_NB "-n"
# define FLAG_ADDR "-a"
# define FLAG_BLOC "-ctmo"
# define EXT_BIN ".cor"

int	load(t_data *data, int ac, char **av, char *path[]);

#endif
