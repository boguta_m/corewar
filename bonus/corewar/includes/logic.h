/*
** logic.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Mar  2 17:08:12 2014 camill_n
** Last up Sun Mar  2 17:12:21 2014 camill_n
*/

#ifndef LOGIC_H_
# define LOGIC_H_

int	and(t_data *data, t_process *proc);
int	or(t_data *data, t_process *proc);
int	xxor(t_data *data, t_process *proc);

#endif
