/*
** error_arg.c for corewar in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Mar  2 17:08:12 2014 camill_n
** Last up Sun Mar  2 17:12:21 2014 camill_n
*/

#ifndef ERROR_ARG_H_
# define ERROR_ARG_H_

void	error_arg(char *name);
void	error_dump(int mode);
void	error_champ(int mode, char *s1, char *s2);

#endif
