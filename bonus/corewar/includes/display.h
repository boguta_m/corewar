#ifndef DISPLAY_H_
# define DISPLAY_H_

# include <SDL/SDL.h>
# include <SDL/SDL_ttf.h>

# define GRILLE_LAR	128
# define GRILLE_HAU	48

typedef struct	s_display
{
  SDL_Surface	*ecran;
  SDL_Surface	*player[5];
  SDL_Surface	*footer;
  SDL_Surface	*pc[4];
  SDL_Surface	*grille[GRILLE_LAR][GRILLE_HAU];
  SDL_Rect	position;
  SDL_Rect	postext;
  SDL_Rect	pos_footer;
  TTF_Font	*police;
  SDL_Color	color;
}		t_display;

t_display	*init_vid();
int		display(t_data *data, t_display *disp, int dump);

#endif
