/*
** my_match.c for my_match in /home/boguta_m/rendu/lib/srcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Nov 21 16:05:49 2013 maxime boguta
** Last update Fri Nov 22 13:47:55 2013 maxime boguta
*/

int	my_match(char tofind, char *toseek)
{
  int	i;

  i = 0;
  while (toseek && (toseek[i] != 0 && toseek[i] != tofind))
    i++;
  if (toseek[i] == 0)
    return (-1);
  return (i);
}
