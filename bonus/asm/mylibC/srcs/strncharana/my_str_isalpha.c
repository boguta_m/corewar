/*
** my_str_isalpha.c for my_str_isalpha in /home/boguta_m/rendu/Piscine-C-Jour_06
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Mon Oct  7 22:00:20 2013 maxime boguta
** Last update Thu Nov 28 16:24:19 2013 maxime boguta
*/

int	my_str_isalpha(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if ((str[i] < 'a' && str[i] > 'Z') || (str[i] < 'A' || str[i] > 'z'))
	return (0);
      i = i + 1;
    }
  return (1);
}
