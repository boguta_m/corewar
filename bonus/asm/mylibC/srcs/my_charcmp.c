/*
** my_charcmp.c for my_charcmp in /home/boguta_m/rendu/lib/srcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Thu Nov 28 16:54:40 2013 maxime boguta
** Last update Thu Nov 28 18:38:53 2013 maxime boguta
*/

int	my_charcmp(char s1, char s2)
{
  if (s1 < s2)
    return (-1);
  if (s1 > s2)
    return (1);
  return (0);
}
