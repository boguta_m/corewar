/*
** my_alphcmp.c for my_alphcmp in /home/boguta_m/rendu/lib/srcs
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Nov 28 16:10:12 2013 maxime boguta
** Last update Thu Dec  5 02:21:10 2013 maxime boguta
*/

int	my_alphcmp(char *s1, char *s2)
{
  int	i;
  int	k;

  k = -1;
  i = -1;
  while (my_char_isalpha(s1[++i]) == 0 && s1[i]);
  while (my_char_isalpha(s2[++k]) == 0 && s2[k]);
  if (s1[i] == 0)
    {
      if (s1[0] == '.')
	return (-1);
      return (my_strcmp(s1, s2));
    }
  else
    while (s1[i])
      {
	if (s1[i] == s2[k] - 32 || (s1[i] == s2[k] + 32) || s1[i] == s2[k])
	  {
	    i++;
	    k++;
	  }
	else
	  return (my_alphacharcmp(s1[i], s2[k]));
      }
  return (0);
}
