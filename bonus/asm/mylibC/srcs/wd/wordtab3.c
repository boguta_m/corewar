/*
** wordtab.c for mysh in /home/camill_n/rendu/PSU_2013_minishell1
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Mon Dec  9 00:24:36 2013 Nicolas Camilli
** Last update Sat Mar 22 22:07:16 2014 Maxime Boguta
*/

#include "global.h"

int	get_nb_word_e(char *str, char c)
{
  int	i;
  int	cpt;

  cpt = 1;
  i = 0;
  while (str[i] != '\0')
    {
      (str[i] == c && str[i + 1] != c) ? ++cpt : 0;
      ++i;
    }
  return (cpt);
}
