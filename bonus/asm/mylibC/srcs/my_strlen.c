/*
** my_strlen.c for my_strlen in /home/boguta_m/rendu/Piscine-C-Jour_04
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Fri Oct  4 12:42:26 2013 maxime boguta
** Last update Mon Jan  6 16:09:04 2014 Maxime Boguta
*/

#include <stdlib.h>

int	my_strlen(char *str)
{
  int	i;

  i = -1;
  if (str != NULL)
    {
      while (str[++i] != 0);
      return (i);
    }
  else
    return (-1);
}
