/*
** my_getenv.c for my_getenv in /home/boguta_m/rendu/mylibC/srcs
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Wed Jan 15 16:00:13 2014 Maxime Boguta
** Last update Wed Feb  5 17:34:59 2014 Maxime Boguta
*/

#include <stdlib.h>

char	*my_getenv(char *env, char *envp[])
{
  int	i;

  i = 0;
  while (my_strncmp(envp[i], env, my_strlen(env)) != 0 && envp[i] &&
	 envp[i][my_strlen(env)] == '=')
    i++;
  if (envp[i])
    return (&envp[i][my_strlen(env) + 1]);
  else
    return (NULL);
}
