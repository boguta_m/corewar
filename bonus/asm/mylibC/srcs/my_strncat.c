/*
** my_strncat.c for my_strncat in /home/boguta_m/rendu/Piscine-C-Jour_07/ex_02
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Wed Oct  9 21:59:09 2013 maxime boguta
** Last update Sat Dec 21 19:11:01 2013 Maxime Boguta
*/

int	my_strncat(char *dest, char *src, int size)
{
  int	i;
  int	j;
  int	dest_len;

  j = 0;
  i = 0;
  while (dest[i] != '\0')
    {
      i = i + 1;
    }
  while (src[j] != '\0' && j != size)
    {
      dest[i] = src[j];
      j = j + 1;
      i = i + 1;
    }
  return (0);
}
