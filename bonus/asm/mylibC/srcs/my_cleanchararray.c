/*
** my_cleanchararray.c for my_cleanchararray in /home/boguta_m/rendu/PSU_2013_my_select/mylibC/srcs
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Wed Jan 15 18:03:24 2014 Maxime Boguta
** Last update Wed Jan 15 18:06:23 2014 Maxime Boguta
*/

void	my_ccarray(char *c[], int size)
{
  int	i;

  i = 0;
  while (i < size)
    *c[i] = 0;
}
