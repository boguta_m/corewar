/*
** my_calloc.c for my_calloc in /home/boguta_m/rendu/lib/srcs
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Nov 21 16:23:49 2013 maxime boguta
** Last update Thu Jan 16 19:26:04 2014 Maxime Boguta
*/

#include <stdlib.h>

void		*my_calloc(int size, char fill)
{
  char		*ptr;
  void		*save;

  if ((ptr = malloc(size + 1)) == NULL)
    return (NULL);
  save = ptr;
  ptr[size] = 0;
  size--;
  while (size >= 0)
    {
      ptr[size] = fill;
      size--;
    }
  return (save);
}
