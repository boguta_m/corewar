/*
** my_getnbr.c for my_getnbr in /home/boguta_m/rendu/Piscine-C-Jour_04
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Fri Oct  4 21:04:53 2013 maxime boguta
** Last update Thu Apr 10 16:56:55 2014 Maxime Boguta
*/

#include <stdlib.h>
#include "my.h"

float	get_numberf(char *str, int i, int neg)
{
  float	nb;
  float	post;
  float	div;
  short	af;

  div = 1;
  nb = 0;
  af = 0;
  post = 0;
  while (str[i] && str[i] >= '0' && str[i] <= '9' || str[i] == ',' ||
	 str[i] == '.')
    {
      if (str[i] == ',' || str[i] == '.')
	{
	  i++;
	  af = 1;
	}
      else if (af == 0)
	nb = nb * 10 + (str[i++] - '0');
      if (af == 1 && ((div *= 10) == div))
	post += (float)(str[i++] - '0') / div;
    }
  if (neg % 2 == 1)
    return (-(nb + post));
  return (nb + post);
}

float	my_getnbrf(char *str)
{
  int	i;
  float	nb;
  int	neg;

  if (str == NULL)
    return (0);
  i = 0;
  neg = 0;
  while (str[i])
    {
      if (str[i] == '-')
	neg++;
      if (str[i] >= '0' && str[i] <= '9')
	{
	  nb = get_numberf(str, i, neg);
	  return (nb);
	}
      i++;
    }
  return (0);
}
