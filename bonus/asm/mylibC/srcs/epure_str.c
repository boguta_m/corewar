/*
** epure_str.c for epure_str in /home/boguta_m/rendu/CPE_2013_corewar/asm/mylibC/srcs
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Wed Mar 26 17:05:22 2014 Maxime Boguta
** Last update Thu Apr 10 17:07:10 2014 Maxime Boguta
*/

#include <stdlib.h>
#include "my.h"

char	*epure_str(char *s)
{
  int	i;
  int	j;
  char	*n;

  i = -1;
  j = -1;
  n = my_strdup(s);
  while (n[++i])
    {
      if (j == -1 && (n[i] == ' ' || n[i] == '\t'));
      else if ((n[i] == ' ' || n[i] == '\t')
	       && ((n[i + 1] == ' ' || n[i + 1] == '\t') || n[i + 1] == 0));
      else
	s[++j] = n[i];
    }
  s[j + 1] = 0;
  free(n);
  return (s);
}
