/*
** my_printf.c for my_printf in /home/boguta_m/Projets/PSU_2013_my_printf/srcs
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Tue Nov 12 09:49:04 2013 maxime boguta
** Last update Thu Jan 16 22:03:05 2014 Maxime Boguta
*/

#include <stdarg.h>
#include <stdlib.h>
#include "def.h"

int		check_4wide(char *str, int i, t_flags *flags)
{
  while (IS_A_NUM(str[i]))
    {
      if (IS_A_NUM(str[i - 1]))
	flags->wide = flags->wide * 10;
      flags->wide = flags->wide + str[i] - '0';
      i++;
    }
  return (i);
}

int		compare_in(char *str, int i, t_flags *flags, va_list *ap)
{
  int		j;
  int		k;

  k = 0;
  j = 0;
  k = check_4wide(str, i, flags);
  while (flags->atypes[j] != 0 && (str[i] != flags->atypes[j]))
    j++;
  if (flags->atypes[j] != 0)
    {
      flags->globlen = flags->globlen + flags->f[j](ap, flags);
      k++;
      flags->globlen = flags->globlen - k;
    }
  else
    return (-i);
  return (i);
}

int		print_nmatch(int ret, int i, char *str)
{
  i = i + ret;
  while (str[i] != 0 && str[i] != '%')
    {
      my_putchar(str[i]);
      i++;
    }
  return (i);
}

void		init_var(int *i, t_flags *flags)
{
  *i = 0;
  store_struct(flags);
  store_flags_rp(flags);
}

int		my_printf(char *str, ...)
{
  t_flags	flags;
  int		i;
  int		ret;
  va_list	ap;

  init_var(&i, &flags);
  va_start(ap, str);
  while (str && str[i] != 0)
    {
      if (str[i] == '%')
	{
	  i = i + 1;
	  if (str[i] == '%')
	    my_putchar('%');
	  else if (ret = compare_in(str, i, &flags, &ap) < 0)
	    i = print_nmatch(ret, i, str);
	}
      else
	my_putchar(str[i]);
      i = i + 1;
    }
  free(flags.f);
  free(flags.atypes);
  return (flags.globlen + i);
}
