/*
** repfuncs2.c for my_printf in /home/boguta_m/Projets/PSU_2013_my_printf/srcs/prsrcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Sun Nov 17 16:50:14 2013 maxime boguta
** Last update Sun Nov 17 23:02:45 2013 maxime boguta
*/

#include <string.h>
#include <stdarg.h>
#include "def.h"

int		pr_putstr(va_list *ap, t_flags *flags)
{
  int		j;
  char		*str;

  j = 0;
  str = va_arg(ap, char *);
    {
      while (str && str[j] != 0)
	{
	  write (1, &str[j], 1);
	  j++;
	}
    }
  return (j);
}

int		pr_putchar(va_list *ap, t_flags *flags)
{
  char		c;

  c = va_arg(ap, int);
  write (1, &c, 1);
  return (1);
}

int		my_putnbr_base_c(int nbr, char *base)
{
  long long     div;
  int           len;
  char          c;
  int		ct;

  div = 1;
  ct = 0;
  len = my_strlen(base);
  if (nbr < 0)
    {
      my_putchar('-');
      ct++;
      nbr = -nbr;
    }
  while (nbr / div > 1)
    div = div * len;
  div = div * len;
  while (div > 1)
    {
      div = div / len;
      c = nbr / div % len;
      my_putchar(base[c]);
      ct++;
    }
  return (ct);
}

int		pr_oct(va_list *ap, t_flags *flags)
{
  return (my_putnbr_base(va_arg(ap, int), "012345678"));
}
