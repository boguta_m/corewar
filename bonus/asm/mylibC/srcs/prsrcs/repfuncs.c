/*
** repfuncs.c for my_printf in /home/boguta_m/Projets/PSU_2013_my_printf/srcs/prsrcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Fri Nov 15 18:04:55 2013 maxime boguta
** Last update Sun Nov 17 19:08:31 2013 maxime boguta
*/

#include <stdarg.h>
#include "def.h"

int	my_put_nbrbigfilter_c(int result, int div, int nb, int *cp)
{
  if (nb != -2147483648 || div != -1)
    {
      result = nb / div;
      result = result % 10;
      result = result + '0';
      my_putchar(result);
      *cp++;
    }
  else
    {
      my_putchar('8');
      *cp++;
    }
}

int	my_put_nbrfunc_c(int result, int div, int isneg, int nb)
{
  int	cp;

  cp = 0;
  div = -div;
  while (div >= nb && div > -1000000000)
    div = div * 10;
  if (div < -1 && div > -1000000000)
    div = div / 10;
  while (div != 0)
    {
      if (isneg >= 1)
	{
	  my_putchar('-');
	  isneg = 0;
	  cp++;
	}
      my_put_nbrbigfilter_c(result, div, nb, &cp);
      div = div / 10;
    }
  return (cp);
}

int	my_put_nbr_c(int nb)
{
  int   result;
  int   div;
  char  isneg;

  result = 0;
  div = 1;
  isneg = 0;
  if (nb >= 0)
    nb = -nb;
  else
    {
      isneg = 1;
    }
  return (my_put_nbrfunc_c(result, div, isneg, nb));
}

int	pr_putnbr(va_list *ap, t_flags *flags)
{
  int	ret;

  ret = my_put_nbr_c(va_arg(ap, int));
  return (ret);
}
