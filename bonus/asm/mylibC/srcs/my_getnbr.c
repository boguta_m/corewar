/*
** my_getnbr.c for my_getnbr in /home/boguta_m/rendu/Piscine-C-Jour_04
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Fri Oct  4 21:04:53 2013 maxime boguta
** Last update Sat Apr 12 16:20:13 2014 Maxime Boguta
*/

#include <stdlib.h>

int	get_number(char *str, int i, int neg)
{
  int	nb;
  int	save;

  nb = 0;
  while (str[i] && str[i] >= '0' && str[i] <= '9')
    {
      save = nb;
      nb = nb * 10 + (str[i++] - '0');
    }
  if (neg % 2 == 1)
    nb = nb * -1;
  return (nb);
}

int	my_getnbr(char *str)
{
  int	i;
  int	neg;

  if (str == NULL)
    return (0);
  i = 0;
  neg = 0;
  while (str[i])
    {
      if (str[i] == '-')
	neg++;
      if (str[i] >= '0' && str[i] <= '9')
	return (get_number(str, i, neg));
      i++;
    }
  return (0);
}
