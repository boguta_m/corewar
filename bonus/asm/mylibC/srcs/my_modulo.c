/*
** my_modulo.c for my_modulo in /home/boguta_m/rendu/mylibC/srcs
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Wed Jan  8 18:28:49 2014 Maxime Boguta
** Last update Wed Jan  8 18:35:01 2014 Maxime Boguta
*/

double	my_modulo(double x, double y)
{
  x -= y * abs(x / y);
  if (x >= 0.)
    return (x);
  else
    return (x + y);
}
