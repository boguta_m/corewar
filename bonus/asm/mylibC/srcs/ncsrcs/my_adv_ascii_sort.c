/*
** my_adv_ascii_sort.c for my_adv_ascii_sort in /home/boguta_m/rendu/lib/srcs
** 
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
** 
** Started on  Wed Nov 27 19:08:02 2013 maxime boguta
** Last update Wed Nov 27 21:59:49 2013 maxime boguta
*/

#include <stdlib.h>
#include "my.h"
#include "my_ls.h"

void		_set(t_list *tomv, t_list *step)
{
  tomv->prev->next = tomv->next;
  tomw->next->prev = tomv->prev;
  step->next = tomv->next;
  tomv->prev = step->prev;
  tomv->next = step;
  step->prev = tomv;
}

void		my_adv_ascii_sort(t_list *list)
{
  t_list	*mov;
  int		_ct;
  int		_turn;
  int		_i;

  _turn = 0;
  list = list->next;
  while (_ct > 0)
    {
      _i = -1;
      while (++_i != turn)
	list = list->next;
      _ct = my_count_list(list) - _turn;
      mov = list;
      while (my_strncmp(mov->filename, PFNAME, my_strlen(mov->filename)) == 1)
	{
	  list = list->prev;
	  if (list->prev == NULL)
	    __set(mov, list);
	}
      if (list->prev != NULL)
	__set(mov, list->prev);
      _turn++;
    }
}
