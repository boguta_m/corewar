/*
** common.h for corewar in /home/boguta_m/rendu/CPE_2013_corewar/asm/includes
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Tue Mar  4 13:22:56 2014 Maxime Boguta
** Last update Mon Mar 24 13:27:46 2014 Maxime Boguta
*/

#ifndef	COMMON_H_
# define COMMON_H_

# include "op.h"

typedef struct		s_label
{
  int			pos;
  int			num;
  char			*name;
  struct s_label	*next;
  struct s_label	*prev;
}			t_label;

typedef struct	s_arg
{
  int		aflag;
  int		val;
}		t_arg;

typedef struct	s_line
{
  int		iflag;
  t_arg		arg[4];
}		t_line;

typedef struct		s_glo
{
  t_line		line;
  t_label		*label;
  op_t			op[17];
  char			*file;
  char			*output;
  int			fdout;
  int			lstart;
}			t_glo;

#endif
