/*
** complement.h for my_select in /home/camill_n/rendu/PSU_2013_my_select
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Fri Jan 10 15:21:05 2014 Nicolas Camilli
** Last update Tue Mar  4 15:58:08 2014 Maxime Boguta
*/

#ifndef COMPLEMENT_H_
# define COMPLEMENT_H_

char	*my_strcat(char *s1, char *s2);
void	my_strcatt(char *s1, char *s2);
int	is_alnum(char *str);
int	my_strncmp(char *s1, char *s2, int i, int j);
int	t_putchar(int c);

#endif
