/*
** wordtab.h for wordtab in /home/camill_n/rendu/PSU_2013_minishell1
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu Jan 23 14:09:01 2014 camill_n
** Last update Wed Mar 26 17:31:46 2014 Maxime Boguta
*/

#ifndef WORDTAB_H_
# define WORDTAB_H_

int	get_nb_word(char *str, char c);
void	my_free_wordtab(char **tab);
void	my_show_wordtab(char **tab);
int	get_sizetab(char **tab);
char	**my_wordtab(char *req, char sep);
char	**my_wordtab_e(char *req, char sep);
char	**duptab(char **tab);
char	*my_strncatt(char *, int, int);
void	*myxlloc(int);

#endif
