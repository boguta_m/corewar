/*
** def.h for my_printf in /home/boguta_m/Projets/PSU_2013_my_printf
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Tue Nov 12 18:33:04 2013 maxime boguta
** Last update Thu Feb 20 13:10:24 2014 Maxime Boguta
*/

#ifndef DEF_H_
# define DEF_H_
# define IS_A_NUM(x) (x) >= '0' && (x) <= '9'
# define F flags->f

typedef struct s_flags {
  int	wide;
  char	*atypes;
  int	globlen;
  int	(** f)(va_list *ap, struct s_flags *flags);
} t_flags;

int	pr_putnbr(va_list *ap, t_flags *flags);
int	pr_putstr(va_list *ap, t_flags *flags);
int	pr_putchar(va_list *ap, t_flags *flags);
int	pr_oct(va_list *ap, t_flags *flags);
int	pr_bin(va_list *ap, t_flags *flags);
int	pr_hexlow(va_list *ap, t_flags *flags);
int	pr_hexup(va_list *ap, t_flags *flags);
int	pr_hexmaj(va_list *ap, t_flags *flags);
int	pr_stroct(va_list *ap, t_flags *flags);
int	pr_putnbr_u(va_list *ap, t_flags *flags);
int	pr_hexptr(va_list *ap, t_flags *flags);

#endif
