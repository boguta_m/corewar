/*
** name2.c for asm in /home/boguta_m/rendu/CPE_2013_corewar/asm
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Wed Mar 26 17:57:04 2014 Maxime Boguta
** Last update Sun Apr 13 19:51:00 2014 Maxime Boguta
*/

#include <stdlib.h>
#include "my.h"
#include "wordtab.h"
#include "op.h"
#include "common.h"

char		**go_name(int fd, t_glo *glo)
{
  char		*line;
  char		**wd;

  wd = NULL;
  while ((line = get_next_line(fd, 0)) != NULL)
    {
      if (_name_done(wd = my_wordtab(line, ' ')) != 0)
	break;
      my_free_wordtab(wd);
      free(line);
    }
  my_free_wordtab(wd);
  if (line == NULL)
    exit_and_rem("error : can't get name : file is incomplete or empty",
		 1, glo);
  wd = my_wordtab_e(line, '"');
  free(line);
  return (wd);
}

char		**go_com(int fd, t_glo *glo)
{
  char		*line;
  char		**wd;

  wd = NULL;
  while ((line = get_next_line(fd, 0)) != NULL &&
	 (((wd = my_wordtab_e(line, '"')) == NULL) || skip_useless(wd) == 1))
    {
      if (wd)
	my_free_wordtab(wd);
      if (line)
	free(line);
    }
  if (line == NULL)
    exit_and_rem("error : can't get comment : file is incomplete or empty",
		 1, glo);
  free(line);
  return (wd);
}

void		get_params(int fd, int fdout, t_glo *glo)
{
  char		**wd;

  header_init(&glo->header);
  wd = go_name(fd, glo);
  get_name(wd, &glo->header, glo);
  wd = go_com(fd, glo);
  get_com(wd, &glo->header, glo);
  if (glo->write == 1)
    set_header(fdout, &glo->header, glo);
}
