/*
** name.c for name in /home/camill_n/rendu/CPE_2014_corewar
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Mar  4 13:39:15 2014 camill_n
** Last update Sat Apr 12 14:36:55 2014 Maxime Boguta
*/

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "wordtab.h"
#include "op.h"
#include "parse.h"
#include "common.h"

void	set_header(int fd, header_t *hea, t_glo *glo)
{
  hea->prog_size = rev_endian(glo->pc);
  hea->magic = rev_endian(COREWAR_EXEC_MAGIC);
  write(fd, hea, sizeof(header_t));
  glo->pc = 0;
}

int	_name_done(char **l)
{
  if (l == NULL)
    return (0);
  else if (l[0] == NULL)
    return (0);
  else if (l[0][0] == '#')
    return (0);
  else
    return (1);
  if (l)
    my_free_wordtab(l);
}

void	get_name(char **line, header_t *hea, t_glo *glo)
{
  int	i;

  i = -1;
  if (line[0])
    epure_str(line[0]);
  if (line[0] == NULL || my_strcmp(line[0], NAME_CMD_STRING) != 0)
    exit_and_rem("error : champion name is badly formated or not given",
		 1, glo);
  if (line[1])
    epure_str(line[1]);
  if (line[1] == NULL || my_strlen(line[1]) < 1)
    exit_and_rem("error : champion name is empty or not given", 1, glo);
  while (line[1][++i] != 0)
    hea->prog_name[i] = line[1][i];
  my_free_wordtab(line);
}

void	get_com(char **line, header_t *hea, t_glo *glo)
{
  int	i;

  i = -1;
  if (line[0])
    epure_str(line[0]);
  if (line[0] == NULL || my_strcmp(line[0], COMMENT_CMD_STRING) != 0)
    return ;
  if (line[1])
    epure_str(line[1]);
  if (line[1] == NULL || my_strlen(line[1]) < 1)
    exit_and_rem("error : champion comment is empty or not given", 1, glo);
  while (line[1][++i] != 0)
    hea->comment[i] = line[1][i];
  my_free_wordtab(line);
}
