/*
** errors2.c for corewar in /home/boguta_m/rendu/CPE_2013_corewar/asm
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Apr 10 16:22:19 2014 Maxime Boguta
** Last update Thu Apr 10 16:44:57 2014 Maxime Boguta
*/

#include <stdlib.h>
#include "common.h"

void	exit_and_rem(char *line, int err, t_glo *glo)
{
  my_printf("%s\n", line);
  free_var(glo);
  exit(err);
}
