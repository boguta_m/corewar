/*
** labels2.c for corewar in /home/boguta_m/rendu/CPE_2013_corewar/asm/src
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Mon Apr  7 12:54:16 2014 Maxime Boguta
** Last update Wed Apr  9 18:37:42 2014 Maxime Boguta
*/

#include "global.h"

int		se_for_label(t_glo *glo, char *name, int l)
{
  t_label	*sav;
  char		*cmp;

  sav = glo->label;
  cmp = my_strdup(name);
  cmp[my_strlen(cmp) - 1] = 0;
  while (sav && my_strcmp(cmp, sav->name) != 0)
    sav = sav->prev;
  if (sav != NULL)
    {
      my_printf("error : multiple definition of label '%s' ", cmp);
      exit_line("at line", l, 1, glo);
    }
  free(cmp);
}

int		get_lab_value(t_glo *glo, char *arg, int l)
{
  int		i;
  t_label	*sav;

  i = -1;
  while (arg[++i] != LABEL_CHAR && arg[i] != 0);
  if (arg[i] == 0)
    return (-1);
  sav = glo->label;
  while (sav && my_strcmp(sav->name, &arg[i + 1]) != 0)
    {
      sav = sav->prev;
    }
  if (sav == NULL)
    exit_line("error : calling to an undefined label name at line", l, 1, glo);
  return (sav->pos - glo->pc);
}

int	label_ok(char **line, int l, t_glo *glo)
{
  int	size;
  int	i;
  int	ok;

  ok = 0;
  if (line && line[0] &&
      line[0][(size = my_strlen(line[0])) - 1] == LABEL_CHAR)
    {
      size--;
      while (size-- > 0)
	{
	  ok = 0;
	  chk_lab_chars(line, l, size, glo);
	  ok = 1;
	}
    }
  return (ok);
}
