/*
** main_parse.c for corewar in /home/boguta_m/rendu/CPE_2013_corewar
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Tue Mar  4 13:17:23 2014 Maxime Boguta
** Last update Thu Apr 10 16:58:59 2014 Maxime Boguta
*/

#include <sys/types.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include "common.h"
#include "parse.h"

void		_parse(t_glo *glo)
{
  int		fd;
  char		*line;
  int		l;

  l = 1;
  line = NULL;
  if ((fd = open(glo->file, O_RDONLY)) == -1)
    exit_msg("error : file either doesn't exist or can't be read", 1);
  if (glo->write == 1)
    if ((glo->fdout = open(glo->output, O_CREAT
			   | O_WRONLY | O_TRUNC, 00644)) == -1)
      exit_msg("error : output file can't be created nor replaced", 1);
  get_params(fd, glo->fdout, glo);
  get_next_line(fd, 1);
  close(fd);
  fd = open(glo->file, O_RDONLY);
  get_labels(fd, glo);
  fd = open(glo->file, O_RDONLY);
  get_next_line(fd, 1);
  while ((line = get_next_line(fd, 0)) != NULL)
    {
      if (read_line(glo, line, l++) == 0 && glo->write == 1)
	write_line(glo);
      reinit_flags(glo, line);
    }
}

void		free_var(t_glo *glo)
{
  t_label	*mv;

  mv = glo->label;
  free(glo->output);
  while (glo->label != NULL)
    {
      free(glo->label->name);
      glo->label = glo->label->prev;
      free(mv);
      mv = glo->label;
    }
  free(mv);
}

void		main_parse(t_glo *glo)
{
  init_parse(glo);
  _parse(glo);
}
