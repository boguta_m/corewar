/*
** read.c for corewar in /home/boguta_m/rendu/CPE_2013_corewar/asm/src
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Mon Apr  7 12:48:59 2014 Maxime Boguta
** Last update Sat Apr 12 16:20:25 2014 Maxime Boguta
*/

#include "global.h"
#include "ext.h"

int		get_value(t_glo *glo, char *arg, args_type_t type, int l)
{
  int		labval;

  labval = get_lab_value(glo, arg, l);
  if (type == 1 || type == 32)
    return (my_getnbr(&arg[1]));
  if (type == 2 || type == 16 && labval == -1)
    return (my_getnbr(&arg[1]));
  if (type == 4 && labval == -1)
    return (my_getnbr(&arg[0]));
  if (labval != -1)
    return (labval);
}
void		get_args(int op, char **wt, t_glo *glo, int l)
{
  int		i;
  args_type_t	type;

  i = -1;
  while (++i < op_tab[op - 1].nbr_args)
    {
      type = get_type(wt[i], 0);
      type = spe_type(i, op, type);
      glo->line.arg[i].aflag = type;
      glo->line.arg[i].val = get_value(glo, wt[i], type, l);
    }
}

char		**set_values(t_glo *glo, char **wt, int l)
{
  int		i;

  i = -1;
  while (++i < 16 && my_strcmp(op_tab[i].mnemonique, wt[0]) != 0);
  glo->line.iflag = op_tab[i].code;
  wt++;
  get_args(op_tab[i].code, wt, glo, l);
  glo->pc += get_size(wt, i, glo);
  return (wt);
}

 int		read_line(t_glo *glo, char *line, int l)
{
  char		**wt;
  char		**wtp;
  int		ret;

  ret = 0;
  if (line == NULL || line[0] == 0 || skip_comments(line) == 1)
    return (1);
  wt = my_wordtab(line, SEPARATOR_CHAR);
  wtp = skip_labels(wt);
  if (wtp[0] != 0)
    set_values(glo, wtp, l);
  else
    ret = 1;
  if (wt[0])
    my_free_wordtab(wt);
  else if (wt)
    free(wt);
  return (ret);
}
