/*
** write.c for asm in /home/bousca_a/Documents/asm/src
**
** Made by antonin bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Tue Mar  4 13:42:08 2014 antonin bouscarel
** Last update Sat Apr 12 16:20:40 2014 Maxime Boguta
*/

#include <stdlib.h>
#include "op.h"
#include "my.h"
#include "common.h"

int	set_param(char *param, int flag, int rank)
{
  if (flag == 4)
    flag = 3;
  *param += (flag & 1) << (7 - (rank * 2) - 1);
  flag >>= 1;
  *param += (flag & 1) << (7 - (rank * 2));
}

unsigned char	add_oc_cod(t_glo *all, int *size)
{
  unsigned char param;
  int		k;

  param = 0;
  k = 0;
  while (k < 4)
    {
      if (all->line.arg[k].aflag == T_IND_DIR)
	set_param(&param, T_DIR, k);
      else if (all->line.arg[k].aflag == T_IND_REG)
	set_param(&param, T_REG, k);
      else
	set_param(&param, all->line.arg[k].aflag, k);
      AFLAG == T_REG ? *size += 1 : 0;
      AFLAG == T_DIR ? *size += 4 : 0;
      AFLAG == T_IND ? *size += 2 : 0;
      AFLAG == T_IND_DIR ? *size += 2 : 0;
      AFLAG == T_IND_REG ? *size += 1 : 0;
      k++;
    }
  return (param);
}

int		add_data_in_line(unsigned char *line, int val, int size, int *j)
{
  int		i;
  int		k;

  i = 0;
  size != 1 ? *j += (size - 1) : 0;
  while (i < size)
    {
      line[*j - i] = (unsigned char)(val);
      val >>= 8;
      ++i;
    }
  *j += 1;
  return (0);
}

int		check_flag(t_glo *all, int *size)
{
  if (all->line.iflag == 1 || all->line.iflag == 9 ||
      all->line.iflag == 12)
    *size -= 1;
  return (all->line.iflag == 1 || all->line.iflag == 9 ||
	  all->line.iflag == 12 ? 1 : 2);
}

int		write_line(t_glo *all)
{
  unsigned char	*line;
  int		i;
  int		k;
  int		size;
  int		j;

  k = 0;
  i = 2;
  size = 2;
  line = _init_line(line);
  line[0] = all->line.iflag;
  line[1] = add_oc_cod(all, &size);
  i = check_flag(all, &size);
  while (all->line.arg[k].aflag != 0)
    {
      AFLAG == T_REG ? add_data_in_line(line, VAL, 1, &i) : 0;
      AFLAG == T_IND ? add_data_in_line(line, VAL, 2, &i) : 0;
      AFLAG == T_DIR ? add_data_in_line(line, VAL, 4, &i) : 0;
      AFLAG == T_IND_DIR ? add_data_in_line(line, VAL, 2, &i) : 0;
      AFLAG == T_IND_REG ? add_data_in_line(line, VAL, 1, &i) : 0;
      k++;
    }
  send_data(line, size, all);
  free(line);
  return (0);
}
