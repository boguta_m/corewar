/*
** tools.c for corewar in /home/boguta_m/rendu/CPE_2013_corewar/asm
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Mon Mar 24 13:15:40 2014 Maxime Boguta
** Last update Sat Apr 12 18:26:28 2014 Maxime Boguta
*/

#include <stdlib.h>
#include "wordtab.h"
#include "op.h"

int	rev_endian(int nb)
{
  int	new;

  new = 0;
  new += (nb & 0xff000000) >> 24;
  new += (nb & 0x000000ff) << 24;
  new += (nb & 0x0000ff00) << 8;
  new += (nb & 0x00ff0000) >> 8;
  return (new);
}

int	skip_useless(char **wt)
{
  if (wt != NULL && wt[0] != NULL && wt[0][0] != COMMENT_CHAR)
    return (0);
  return (1);
}

int	skip_comments(char *l)
{
  char	**wd;

  if (!l || l[0] == COMMENT_CHAR)
    return (1);
  wd = my_wordtab(l, SEPARATOR_CHAR);
  if (wd[0] == NULL)
    {
      my_free_wordtab(wd);
      return (1);
    }
  else if (my_strcmp(NAME_CMD_STRING, wd[0]) == 0 ||
      my_strcmp(COMMENT_CMD_STRING, wd[0]) == 0)
    {
      my_free_wordtab(wd);
      return (1);
    }
  else if (wd[0][0] == COMMENT_CHAR)
    {
      my_free_wordtab(wd);
      return (1);
    }
  my_free_wordtab(wd);
  return (0);
}

char	**skip_labels(char **l)
{
  if (label_ok(l) == 1)
    {
      return (++l);
    }
  return (l);
}
