/*
** my_put_in_list.c for my_put_in_list in /home/boguta_m/rendu/lib/srcs
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sun Nov 24 19:16:57 2013 maxime boguta
** Last update Sun Apr  6 11:50:03 2014 Maxime Boguta
*/

#include <stdlib.h>
#include "common.h"

t_label		*my_vladimir_put_in_list(t_label *in)
{
  t_label	*elem;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (NULL);
  if (in == NULL)
    {
      in = elem;
      elem->next = NULL;
      elem->prev = NULL;
       return (elem);
    }
  while (in->next != NULL)
    in = in->next;
  in->next = elem;
  elem->next = NULL;
  elem->prev = in;
  return (elem);
}
