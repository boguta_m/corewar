/*
** size.c for corewar in /home/boguta_m/rendu/CPE_2013_corewar/asm
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sun Apr  6 15:49:25 2014 Maxime Boguta
** Last update Sat Apr 12 18:20:47 2014 Maxime Boguta
*/

#include "global.h"
#include "ext.h"

args_type_t	spe_type(int arg, int op, args_type_t type)
{
  if (op == 11 && arg != 0)
    {
      if (type != 1)
	return (16);
      else
	return (32);
    }
  else if (op == 12 || op == 15 || op == 9)
    {
      if (type != 1)
	return (16);
      else
	return (32);
    }
  else if ((op == 10 || op == 14) && arg != 3)
    {
      if (type != 1)
	return (16);
      else
	return (32);
    }
  return (type);
}

int		ret_size_fr_type(args_type_t type)
{
  if (type == T_REG || type == T_IND_REG)
    return (1);
  else if (type == T_IND)
    return (2);
  else if (type == T_DIR)
    return (4);
  else if (type == 16)
    return (2);
  return (0);
}

args_type_t	get_type(char *arg, t_glo *glo)
{
  if (arg == NULL || arg[0] == 0)
    exit_line("error : expecting more arguments at line", glo->l, 1, glo);
  if (arg[0] == 'r' && arg[1] != 0 && my_str_isnum(&arg[1]) == 1)
    {
      if (my_getnbr(&arg[1]) > REG_NUMBER || my_getnbr(&arg[1]) <= 0)
	exit_line("error : register out of bounds on argument at line",
		  glo->l, 1, glo);
      return (T_REG);
    }
  else if (arg[0] == DIRECT_CHAR)
    {
      if (arg[1] == 0 || (my_str_isnum(&arg[1]) != 1 && arg[1] != LABEL_CHAR))
	exit_line("error : missing definition of direct index at line",
		  glo->l, 1, glo);
      return (T_DIR);
    }
  else if (arg[0] != 0 && (my_str_isnum(&arg[0]) == 1 || arg[0] == LABEL_CHAR))
    return (T_IND);
  return (0);
}

args_type_t	chk_type(int i, int op, t_glo *glo, char **line)
{
  args_type_t	type;
  int		tref;

  tref = op_tab[op].type[i];
  type = get_type(line[i], glo);
  if (type == 0)
    exit_line("error : unexpected argument type at line", glo->l, 1, glo);
  if (tref % 2 == 1 && tref != type && type != T_REG)
    tref -= T_REG;
  if (tref > 1 && tref != type)
    {
      if (tref > 4 && type != T_IND)
	tref -= T_IND;
      if (type != T_DIR && tref != type)
	tref -= T_DIR;
    }
  if (tref != type)
    exit_line("error : unexpected argument type at line", glo->l, 1, glo);
  return (type);
}

int		get_size(char **line, int op, t_glo *glo)
{
  int		i;
  int		tot;
  args_type_t	type;

  i = -1;
  tot = 1;
  type = 0;
  if (!line || line[0] == NULL)
    exit_line("error : expecting arguments at line", glo->l, 1, glo);
  if (op != 0 && op != 8 && op != 11 && op != 14)
    tot += 1;
  while (++i < op_tab[op].nbr_args)
    {
      type = chk_type(i, op, glo, line);
      type = spe_type(i, op + 1, type);
      tot += ret_size_fr_type(type);
    }
  if (line[i] != NULL && line[i][0] != ANNOTATION_CHAR
      && line[i][0] != COMMENT_CHAR)
    exit_line("error : too many arguments on instruction line",
	      glo->l, 1, glo);
  return (tot);
}
