/*
** common.h for corewar in /home/boguta_m/rendu/CPE_2013_corewar/asm/includes
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Tue Mar  4 13:22:56 2014 Maxime Boguta
** Last update Sat Apr 12 16:30:42 2014 Maxime Boguta
*/

#ifndef	COMMON_H_
# define COMMON_H_

# include "op.h"

# define MAX_SIZE 18
# define AFLAG  all->line.arg[k].aflag
# define VAL    all->line.arg[k].val

typedef struct		s_label
{
  int			pos;
  char			*name;
  struct s_label	*next;
  struct s_label	*prev;
}			t_label;

typedef struct	s_arg
{
  args_type_t	aflag;
  int		val;
}		t_arg;

typedef struct	s_line
{
  int		iflag;
  t_arg		arg[4];
}		t_line;

typedef struct		s_glo
{
  t_line		line;
  int			l;
  t_label		*label;
  int			pc;
  char			*file;
  char			*output;
  int			write;
  int			fdout;
  header_t		header;
}			t_glo;

int		_name_done(char **l);
void		_parse(t_glo *glo);
int		add_data_in_line(unsigned char *line, int val, int size, int *j);
unsigned char	add_oc_cod(t_glo *all, int *size);
char		*alloc_and_add(char *line, char *buf);
int		check_flag(t_glo *all, int *size);
void		chk_clean(int clean);
int		chk_if_ins(t_glo *glo, char **line);
char		**chk_if_label(t_glo *glo, char **line, int tot);
void		chk_lab_chars(char **line, int l, int size, t_glo *glo);
args_type_t	chk_type(int i, int op, t_glo *glo, char **line);
void		error_arg(char *name);
void		error_file_path(char *name);
void		error_malloc(char *str);
void		exit_line(char *msg, int line, int errcode, t_glo *glo);
void		exit_msg(char *msg, int errcode);
void		free_var(t_glo *glo);
void		get_args(int op, char **wt, t_glo *glo, int l);
void		get_com(char **line, header_t *hea, t_glo *glo);
int		get_lab_value(t_glo *glo, char *arg, int l);
void		get_labels(int fd, t_glo *glo);
void		get_name(char **line, header_t *hea, t_glo *glo);
char		*get_next_line(int fd, int clean);
void		get_params(int fd, int fdout, t_glo *glo);
int		get_size(char **line, int op, t_glo *glo);
args_type_t	get_type(char *arg, t_glo *glo);
int		get_value(t_glo *glo, char *arg, args_type_t type, int l);
char		**go_com(int fd, t_glo *glo);
char		**go_name(int fd, t_glo *glo);
void		header_init(header_t *hea);
void		init_parse(t_glo *glo);
char		*is_there_n(char **line);
int		label_ok(char **line, int l, t_glo *glo);
int		main(int ac, char **av);
void		main_parse(t_glo *glo);
t_label		*my_vladimir_put_in_list(t_label *in);
int		read_line(t_glo *glo, char *line, int l);
void		reinit_flags(t_glo *glo, char *l);
int		ret_size_fr_type(args_type_t type);
int		se_for_label(t_glo *glo, char *name, int l);
int		send_data(unsigned char *line, int size, t_glo *all);
void		set_header(int fd, header_t *hea, t_glo *glo);
int		set_param(char *param, int flag, int rank);
char		**set_values(t_glo *glo, char **wt, int l);
int		skip_comments(char *l);
char		**skip_labels(char **l);
int		skip_useless(char **wt);
args_type_t	spe_type(int arg, int op, args_type_t type);
void		store_label(t_glo *glo, char *line, int tot, int l);
int		write_line(t_glo *all);
unsigned char	*_init_line(unsigned char *);

#endif
