/*
** get_next_line.h for gnl in /home/boguta_m/rendu/wop
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Mon Mar 24 15:02:00 2014 Maxime Boguta
** Last update Mon Mar 24 15:31:15 2014 Maxime Boguta
*/

#ifndef GNL_H_
# define GNL_H_

# define RD_AMT 512

char	*get_next_line(int, int);

#endif
